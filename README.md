# AquaPyt
AQA Python training

[![CircleCI](https://circleci.com/gh/KOVALKO2/AquaPyt.svg?style=svg)](https://circleci.com/gh/KOVALKO2/AquaPyt)
[![SonarCloud Bugs](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=bugs)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Code smells](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=code_smells)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Coverage](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=coverage)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Duplicated lines](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Lines of code](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=ncloc)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Maintainability](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Quality gate](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=alert_status)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Reliability](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Security](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=security_rating)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Technical debt](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=sqale_index)](https://sonarcloud.io/dashboard?id=AquaPyt)
[![SonarCloud Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=AquaPyt&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=AquaPyt)

- CircleCI dashboard: [https://circleci.com/gh/KOVALKO2/AquaPyt](https://circleci.com/gh/KOVALKO2/AquaPyt)
- SonarCloud dashboard: [https://sonarcloud.io/dashboard?id=AquaPyt](https://sonarcloud.io/dashboard?id=AquaPyt)


# To run SonarCloud analysis
- install sonar-scanner from [https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner)
- run `sonar-scanner` command from root folder

# To run tests
- go to "project" folder
- execute `py.test`

Test project contains next types of tests, which will be executed all by default (previous command).
However, you could run only specific types of tests by executing next commands:
- `py.test -m random` - will execute Random test, which fails 50% of runs (for pytest rerun failures demonstration)
- `py.test -m fibonacci` - will execute unit tests for Fibonacci generator (Phase 1)
- `py.test -m api` - will execute Jira API tests
- `py.test -m ui` - will execute Jira UI tests
- `py.test -m "api ui"` - will execute tests of both mentioned types - example how to run more than 1 type of tests
- `py.test -m "not api ui"` - will execute all tests except of mentioned types


# To see allure report after manual tests execution (not on CI):
- Download the allure package from
[https://bintray.com/qameta/generic/allure2](https://bintray.com/qameta/generic/allure2)
- Unpack it somewhere and add path to bin sub-folder to your PATH
- Run next command in the same folder as previous execution one:
`allure serve ../xunit-reports/allure/files/`

# Reports on CI
- Simple Pytest HTML report: Job details > Artifacts tab > Container 0/test-reports/pytest-html/index.html
- Coverage report: Job details > Artifacts tab > Container 0/test-reports/coverage/report/index.html
- Allure detailed report: Job details > Artifacts tab > Container 0/test-reports/allure/allure-report/index.html
