#!/usr/bin/env python3
"""
Base test with configurations for Fibonacci tests execution
"""
import logging

from project.tests.global_test import GlobalTest
from project.utils.logger_config import setup_logging

logger = logging.getLogger(__name__)


class BaseUnitTest(GlobalTest):
    """
    Main class for Fibonacci tests in project.
    """
    import pytest
    runs = 0


    @pytest.fixture(scope = 'class', autouse = True)
    def fixture_setup(self):
        logger.debug("'fixture_setup' fixture called")
        setup_logging()
