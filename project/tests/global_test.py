#!/usr/bin/env python3
"""
Global class for all tests in project.
"""
import logging

import pytest

from project.utils.config_parser import read_ini_file, read_yaml_file

logger = logging.getLogger(__name__)


class GlobalTest(object):
    """
    Global class for all tests in project.
    """
    from project.utils.http_request import HttpBackend

    API_HTTP = HttpBackend()
    ROUTES = dict(read_yaml_file('tests/data/routes.yaml'))


    @staticmethod
    def _create_issue_params(is_positive: bool = True) -> dict:
        return read_yaml_file('tests/data/create_issue_data.yaml')['positive'] if is_positive \
            else read_yaml_file('tests/data/create_issue_data.yaml')['negative']


    @staticmethod
    def _update_issue_params() -> dict:
        return read_yaml_file('tests/data/update_issue_data.yaml')['positive']


    @staticmethod
    def _update_issue_names(fixture_value: str) -> str:
        return list(dict(fixture_value)['case'].keys())[0]


    @staticmethod
    def _create_issue_names(fixture_value: str) -> str:
        return list(dict(fixture_value)['case'].values())[0]


    def cleanup_issues(self, jql: str):
        """
        Removes all found issues from Jira for current user and provided JQL

        :param jql: JQL str to search for issues, filtering by reporter is already included
        :return: None
        """
        import base64
        import json
        garbage_set = set()
        jql_filter = "reporter={0}&{1}".format(str(read_ini_file('../tests_config.ini')['Jira']['login']), jql)
        self.API_HTTP.session.headers['Content-Type'] = 'application/json'
        response = self.API_HTTP.search(url_part = self.ROUTES['search']['path'], jql = jql_filter)
        if response.status_code == 200 and int(json.loads(response.content)['total']) > 0:
            for issue in json.loads(response.content)['issues']:
                garbage_set.add(issue['id'])
        garbage = list(garbage_set)
        self.API_HTTP.session.headers['Authorization'] = \
            'Basic {}'.format(base64.b64encode(
                    bytes('{0}:{1}'.format(str(read_ini_file('../tests_config.ini')['Jira']['login']),
                                           str(read_ini_file('../tests_config.ini')['Jira']['password'])),
                          'UTF-8')).decode('ascii'))
        for item in garbage:
            logger.debug('Going to delete item with ID = {}'.format(item))
            resp = self.API_HTTP.delete(self.ROUTES['issues']['edit']['path'].format(id = item))
            if resp.status_code == 204:
                garbage_set.discard(item)


    def create_issues(self, prefix: str, purpose_char: str):
        """
        Creates issues with provided prefix and purpose in summaries

        :param prefix: str prefix for issues' summaries
        :param purpose_char: str goes right after prefix
        :return: set(str) with created issues' IDs
        """
        import json
        import base64
        from pprint import pformat
        logger.debug("'fixture_create_issues' fixture called for {} test".format(prefix))
        self.cleanup_issues("summary ~ '{0}{1}*'".format(prefix, purpose_char))
        self.API_HTTP.session.headers['Content-Type'] = 'application/json'
        self.API_HTTP.session.headers['Authorization'] = \
            'Basic {}'.format(base64.b64encode(
                    bytes('{0}:{1}'.format(str(read_ini_file('../tests_config.ini')['Jira']['login']),
                                           str(read_ini_file('../tests_config.ini')['Jira']['password'])),
                          'UTF-8')).decode('ascii'))

        with open('tests/data/create_issue_template.json', 'rt') as file:
            template = json.load(file)
        project_key = read_ini_file('../tests_config.ini')['Jira']['project_key']
        local_set = set()
        for param in GlobalTest._create_issue_params(True):
            data_item = dict(template)
            data_item['fields']['project']['key'] = project_key
            data_item['fields']['summary'] = "{0}{1}{2}".format(prefix, purpose_char, param['case']['summary'])
            data_item['fields']['description'] = \
                param['case']['description'] if param['case']['description'] else ''
            data_item['fields']['issuetype']['name'] = param['case']['name']
            logger.debug('Next JSON was prepared by fixture and issue will be created: \n{}'.format(pformat(data_item)))
            response = self.API_HTTP.post(url_part = self.ROUTES['issues']['create']['path'],
                                          content = json.dumps(data_item))
            try:
                local_set.add(json.loads(response.content)['id'])
            except Exception as ex:
                logger.exception("Failed to parse response content. Seems that object wasn't created. Exception:\n{}"
                                 .format(ex))
        return local_set


    @pytest.fixture()
    def correct_creds(self) -> dict:
        """
        :returns: correct credentials for Jira from config
        """
        logger.debug("'correct_creds' fixture called")
        return {'login'   : str(read_ini_file('../tests_config.ini')['Jira']['login']),
                'password': str(read_ini_file('../tests_config.ini')['Jira']['password'])}


    @pytest.fixture()
    def fixture_login(self, correct_creds):
        import base64
        logger.debug("'fixture_login' fixture called")
        # Just for example if token based auth is used
        # self.api_http.basic_auth(url_part = self.routes['login']['path'],
        #                          login = correct_creds['login'],
        #                          password = correct_creds['password'])
        self.API_HTTP.session.headers['Authorization'] = \
            'Basic {}'.format(base64.b64encode(
                    bytes('{0}:{1}'.format(correct_creds['login'], correct_creds['password']),
                          'UTF-8')).decode('ascii'))


    def get_id_of_issue_to_update(self, login: str, issue_summary_jql: str) -> str:
        """
        Searches in Jira for issue to be updated and returns its ID

        :param issue_summary_jql: str value of issue summary or part of it, acceptable by JQL
        :param login: str Jira login of issue reporter
        :return: str ID of found issue
        """
        import json
        from project.utils.asserts import verify_http_status_code
        from project.utils.attachments import attach_json
        
        self.API_HTTP.session.headers['Content-Type'] = 'application/json'
        response_search_issue = self.API_HTTP.search(
                url_part = self.ROUTES['search']['path'],
                jql = "project = {0} AND summary ~ '{2}' AND issuetype = Bug AND reporter in ({1})"
                    .format(read_ini_file('../tests_config.ini')['Jira']['project_key'], login, issue_summary_jql))
        attach_json(dict(response_search_issue.headers), "Search issue Response headers")
        attach_json(list(response_search_issue.cookies.items()), "Search issue Response cookies")
        verify_http_status_code(response_search_issue.status_code, 200)
        attach_json(json.loads(response_search_issue.content), "Search issue Response body")
        return json.loads(response_search_issue.content)['issues'][0]['id']
