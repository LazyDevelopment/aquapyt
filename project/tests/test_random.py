#!/usr/bin/env python3
"""
Flaky test for reruns demonstration
"""
import logging

import allure
import pytest

from project.tests.base_unit_test import BaseUnitTest
from project.utils.asserts import assertion, greater_than

logger = logging.getLogger(__name__)


@allure.feature('pytest reruns demonstration')
@allure.story('1st execution failing test')
class TestRandom(BaseUnitTest):
    """
    Test that fails for 1st run and passes for all subsequent
    """


    @allure.title("Test that fails for 1st run and passes for all subsequent")
    @pytest.mark.random
    def test_flaky(self):
        """
        Test that fails for 1st run and passes for all subsequent
        """
        logger.info("Test 'flaky' started")
        with allure.step("flaky"):
            logger.info("Step 'flaky' started")
            BaseUnitTest.runs += 1
            assertion("Validate that this is 2nd or subsequent run",
                      BaseUnitTest.runs, greater_than(1),
                      "Oops! This is 1st run and it fails. Next run should pass!")
            logger.info("Step 'flaky' finished")
        logger.info("Test 'flaky' finished")
