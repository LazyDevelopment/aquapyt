#!/usr/bin/env python3
"""
Includes browser tests for login functionality
"""
import os

import pytest

from project.tests.base_ui_test import BaseUiTest
from project.tests.pages.dashboard_page import DashboardPage
from project.tests.pages.login_page import LoginPage
from project.tests.pages.menu_page import MenuPage, UserMenuPage
from project.utils.asserts import *
from project.utils.attachments import *

logger = logging.getLogger(__name__)


def _id(value):
    return value['login']


@allure.story('Jira login')
class TestLogin(BaseUiTest):
    """
    Jira UI login tests
    """
    from project.utils.config_parser import read_ini_file

    _wrong_creds_list = [
        {'login': 'sdfgkjhgbskvyr', 'password': 'sdfvhgskfjhasflaufcbkasjfhbach'},
        {'login': str(read_ini_file('../tests_config.ini')['Jira']['login']), 'password': 'sdfvhgskfjhsjfhbach'},
        {'login': 'alkjdgjsgkshfg', 'password': str(read_ini_file('../tests_config.ini')['Jira']['password'])}
    ]


    @allure.title("UI Login with wrong credentials")
    @pytest.mark.parametrize("creds", _wrong_creds_list, ids = _id)
    def test_login_wrong_creds(self, creds, fixture_provide_webdriver):
        """
        UI Login with wrong credentials
        """
        logger.info("Test 'UI Login with wrong credentials: {}' started".format(creds['login']))
        with allure.step("Opening Jira Login page"):
            logger.info("Step 'Opening Jira Login page' started")
            driver = fixture_provide_webdriver
            login_page = LoginPage(driver).open()
            page_elements = login_page.page_elements
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Opening Jira Login page")
            attach_selenium_request_details(driver.requests, login_page.url, "Login page")
            verify_selenium_responses(driver.requests, ['/rest/zephyr/'])
            del driver.requests
            assertion("The opened page is Login page", login_page.is_login_page(), is_(True), "Wrong page is opened")
            validate_page_elements(page_elements)
            logger.info("Step 'Opening Jira Login page' finished")

        with allure.step("Logging in to Jira with wrong credentials: login = {0}, password = {1}"
                                 .format(creds['login'], creds['password'])):
            logger.info("Step 'Logging in to Jira with wrong credentials: login = {}' started".format(creds['login']))
            login_page.login_to(user = creds['login'], passwd = creds['password'])
            assertion("Login error message is shown", login_page.is_login_error_shown(), is_(True),
                      "There is no error message or text is different")
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Opening Jira Login page")
            attach_selenium_request_details(driver.requests, login_page.url, "Login page")
            verify_selenium_responses(driver.requests, ['/rest/zephyr/'])
            del driver.requests
            logger.info("Step 'Logging in to Jira with wrong credentials: login = {}' finished".format(creds['login']))
        logger.info("Test 'UI Login with wrong credentials: {}' finished".format(creds['login']))


    @allure.title("UI Login with correct credentials")
    def test_login_correct(self, correct_creds, fixture_provide_webdriver):
        """
        UI Login with correct credentials
        """
        logger.info("Test 'UI Login with correct credentials' started")
        with allure.step("Opening Jira Login page"):
            logger.info("Step 'Opening Jira Login page' started")
            driver = fixture_provide_webdriver
            login_page = LoginPage(driver).open()
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Opening Jira Login page")
            attach_selenium_request_details(driver.requests, login_page.url, "Login page")
            logger.info("Step 'Opening Jira Login page' finished")

        with allure.step("Validate Login page content"):
            logger.info("Step 'Validate Login page content' started")
            verify_selenium_responses(driver.requests, ['/rest/zephyr/'])
            del driver.requests
            assertion("The opened page is Login page", login_page.is_login_page(), is_(True), "Wrong page is opened")
            validate_page_elements(login_page.page_elements)
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Validate page content")
            logger.info("Step 'Validate Login page content' finished")

        with allure.step("Logging in to Jira with correct credentials"):
            logger.info("Step 'Logging in to Jira with correct credentials' started")
            login_page.login_to(user = correct_creds['login'], passwd = correct_creds['password'])
            dashboard_page = DashboardPage(driver)
            top_menu_page = MenuPage(driver)
            driver.get_screenshot_as_file(os.path.abspath('../xunit-reports/dashboard-logged-in-{}.png'
                                                          .format(driver.name)))
            attach_screenshot_by_path('../xunit-reports/dashboard-logged-in-{}.png'.format(driver.name),
                                      'Dashboard with logged in user')
            attach_selenium_request_details(driver.requests, login_page.url, "Login action")
            logger.info("Step 'Logging in to Jira with correct credentials' finished")

        with allure.step("Validate Dashboard page content"):
            logger.info("Step 'Validate Dashboard page content' started")
            verify_selenium_responses(driver.requests)
            del driver.requests
            assertion("The opened page is Dashboard page", dashboard_page.is_dashboard_page(), is_(True),
                      "Wrong page is opened")
            assertion("Top menu is present", top_menu_page.is_menu_present(), is_(True),
                      "There is no menu on this page")
            validate_page_elements(dashboard_page.page_elements)
            validate_page_elements(top_menu_page.page_elements)
            top_menu_page.elems.user_profile_menu_collection.click()
            user_menu_page = UserMenuPage(driver)
            assertion("User menu is present", user_menu_page.is_menu_present(), is_(True),
                      "There is no user menu on this page")
            assertion("User is logged in", user_menu_page.is_user_logged_in(), is_(True),
                      "There is no 'Logout' button, so it seems that user is not logged in")
            validate_page_elements(user_menu_page.page_elements)
            attach_screenshot_of_resized_page(driver, "Validate page content")
            logger.info("Step 'Validate Dashboard page content' finished")
        logger.info("Test 'UI Login with correct credentials' finished")
