#!/usr/bin/env python3
"""
UI CRUD Jira issues tests
"""
import pytest

from project.tests.base_ui_test import BaseUiTest
from project.tests.pages.create_issue_page import CreateIssuePage
from project.tests.pages.dashboard_page import DashboardPage
from project.tests.pages.edit_issue_page import EditIssuePage
from project.tests.pages.issues_page import IssuesPage
from project.tests.pages.menu_page import MenuPage, IssuesMenuPage
from project.utils.asserts import *
from project.utils.attachments import *
from project.utils.config_parser import read_ini_file

logger = logging.getLogger(__name__)


@allure.story('Jira issues creation')
class TestIssuesCreate(BaseUiTest):
    """
    Tests for Jira issues UI creation functionality
    """
    _wrong_summary = ['', 'kkb fjslkjvnsdlkurgn;skvnbdsjhvbsvlfjnbvlkjbdngfbdfjbndlbkjvbksjhvbajhgcvckjhvbdvjhgavdkj'
                          'hgcbvkdfhvbvgsvkjhvbdvkdsjhvbsvjhgavakjhabvkjhvssdgvjhgdavjshcgvjhfcvajhcgvjchgvkdjhvbsdk'
                          'jvhbvkjhdsvcjhgcvdjhcgvsjhvbdvkjdhsvsdkvdshgvbdsvkjhgsdjhvgdskvjhsbvdskvjhkjhn']


    @staticmethod
    def _id(summary):
        if summary == '':
            return 'empty-summary'
        else:
            return 'too-long-summary'


    @allure.title("Create issue UI")
    def test_create_issue(self, fixture_provide_webdriver_logged_in, fixture_create_issue_params_positive,
                          fixture_login):
        """
        Create issue UI
        """
        logger.info("Test 'Create issue UI' started")
        driver = fixture_provide_webdriver_logged_in
        DashboardPage(driver).open()

        with allure.step("Creating an issue"):
            logger.info("Step 'Creating an issue' started")
            top_menu_page = MenuPage(driver)
            assertion("Top menu is present", top_menu_page.is_menu_present(), is_(True),
                      "There is no menu on this page")
            top_menu_page.elems.create_issue_button.click()
            create_issue_page = CreateIssuePage(driver)
            create_issue_page.elems.form_caption_txt.wait_to_be_visible()
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Create issue page")
            assertion("The opened page is Create issue page", create_issue_page.is_create_issue_page(), is_(True),
                      "Wrong page is opened")
            validate_page_elements(create_issue_page.page_elements)
            create_issue_page.create_issue(summary =
                                           "UIs{}".format(fixture_create_issue_params_positive['case']['summary']),
                                           issue_type = fixture_create_issue_params_positive['case']['name'])
            # top_menu_page.elems.issue_created_popup_lnk.wait_to_be_clickable()
            top_menu_page.elems.create_issue_button.wait_to_be_clickable()

        with allure.step("Validating created issue"):
            verify_selenium_responses(driver.requests, ['projectavatar'])
            del driver.requests
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response = self.API_HTTP.search(url_part = self.ROUTES['search']['path'], jql = "summary ~ 'UIs{}'"
                                            .format(fixture_create_issue_params_positive['case']['summary']))
            created_issue = None
            if response.status_code == 200 and int(json.loads(response.content)['total']) > 0:
                created_issue = json.loads(response.content)['issues'][0]
            attach_json(json.loads(response.content), "Found issues")
            assertion("1 issue was found by search", int(json.loads(response.content)['total']), is_(1),
                      "No issues found or more than 1 issue found: {}".format(json.loads(response.content)['total']))
            logger.info("Step 'Creating an issue' finished")

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            assertion("Summary is what was provided", created_issue['fields']['summary'],
                      is_("UIs{}".format(fixture_create_issue_params_positive['case']['summary'])),
                      "Summary is wrong: {}".format(created_issue['fields']['summary']))
            assertion("Issue type is what was provided", created_issue['fields']['issuetype']['name'],
                      is_(fixture_create_issue_params_positive['case']['name']),
                      "Issue type is wrong: {}".format(created_issue['fields']['issuetype']['name']))
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'Create issue UI' finished")


    @allure.title("Create issue UI negative test")
    @pytest.mark.parametrize("summary", _wrong_summary)
    def test_create_issue_negative(self, fixture_provide_webdriver_logged_in, summary):
        """
        Create issue UI negative test
        """
        logger.info("Test 'Create issue UI negative test' started")
        driver = fixture_provide_webdriver_logged_in
        DashboardPage(driver).open()

        with allure.step("Creating an issue"):
            logger.info("Step 'Creating an issue' started")
            top_menu_page = MenuPage(driver)
            assertion("Top menu is present", top_menu_page.is_menu_present(), is_(True),
                      "There is no menu on this page")
            top_menu_page.elems.create_issue_button.click()
            create_issue_page = CreateIssuePage(driver)
            create_issue_page.elems.form_caption_txt.wait_to_be_visible()
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Create issue page opened")
            assertion("The opened page is Create issue page", create_issue_page.is_create_issue_page(), is_(True),
                      "Wrong page is opened")
            validate_page_elements(create_issue_page.page_elements)
            create_issue_page.create_issue(summary = summary, should_succeed = False)
            logger.info("Step 'Creating an issue' finished")

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            if summary == '':
                assertion("Error message 'You must specify a summary of the issue.' is shown",
                          create_issue_page.elems.error_txt.get_text(), is_('You must specify a summary of the issue.'),
                          "Error message is not shown or text is different: {}"
                          .format(create_issue_page.elems.error_txt.get_text()))
            else:
                assertion("Error message 'Summary must be less than 255 characters.' is shown",
                          create_issue_page.elems.error_txt.get_text(),
                          is_('Summary must be less than 255 characters.'),
                          "Error message is not shown or text is different: {}"
                          .format(create_issue_page.elems.error_txt.get_text()))
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Create issue page with error triggered")
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'Create issue UI negative test' finished")


@allure.story('Jira issues manipulation')
class TestIssuesManipulate(BaseUiTest):
    """
    Tests for Jira issues manipulation UI functionality (search, update)
    """


    @allure.title("Search UI issues")
    def test_search_issue(self, fixture_provide_webdriver_logged_in, fixture_create_issues_ui):
        """
        Search UI issue
        """
        logger.info("Test 'Search UI issue' started")
        driver = fixture_provide_webdriver_logged_in
        DashboardPage(driver).open()

        with allure.step("Opening list of all my issues"):
            logger.info("Step 'Opening list of all my issues' started")
            top_menu_page = MenuPage(driver)
            assertion("Top menu is present", top_menu_page.is_menu_present(), is_(True),
                      "There is no menu on this page")
            top_menu_page.elems.issues_menu_collection.click()
            issues_menu_page = IssuesMenuPage(driver)
            validate_page_elements(issues_menu_page.page_elements)
            issues_menu_page.go_to_reported_by_me_issues()
            issues_page = IssuesPage(driver)
            assertion("Issues page is opened", issues_page.is_issues_page(), is_(True), "This is wrong page")
            validate_page_elements(issues_page.page_elements)
            attach_screenshot_by_content(driver.get_screenshot_as_png(), "Issues page opened")
            assertion("At least 5 issues are shown", int(issues_page.elems.found_issues_txt.get_text()),
                      greater_than(4), "Not enough issues shown")
            logger.info("Step 'Opening list of all my issues' finished")

        with allure.step("Filtering list of my issues to get only 1"):
            logger.info("Step 'Filtering list of my issues to get only 1' started")
            issues_page.filter_issues("UIbkkb 0123456789")
            assertion("1 issue is shown", int(issues_page.elems.found_issues_txt.get_text()),
                      is_(1), "No issues or more than 1 are shown")
            logger.info("Step 'Filtering list of my issues to get only 1' finished")

        with allure.step("Filtering list of my issues to get no issues"):
            logger.info("Step 'Filtering list of my issues to get no issues' started")
            issues_page.filter_issues("UIbkkbUIbkkbUIbkkb", results_expected = False)
            assertion("Empty results notification is shown", issues_page.elems.empty_results_txt.get_text(),
                      is_("No issues were found to match your search"),
                      "No notification text found or some issues are shown")
            logger.info("Step 'Filtering list of my issues to get no issues' finished")
        logger.info("Test 'Search UI issue' finished")


    @allure.title("Update UI issue")
    def test_update_issue(self, fixture_login, correct_creds, fixture_update_issues_ui, fixture_update_issue_params,
                          fixture_provide_webdriver_logged_in):
        """
        Update UI issue
        """
        logger.info("Test 'Update UI issue' started")
        with allure.step("Getting ID of the issue to be updated"):
            logger.info("Step 'Getting ID of the issue to be updated' started")
            issue_id = self.get_id_of_issue_to_update(correct_creds['login'], 'UIu*')
            logger.info("Step 'Getting ID of the issue to be updated' finished. ID is {}".format(issue_id))

        with allure.step("Updating the issue"):
            logger.info("Step 'Updating the issue' started")
            driver = fixture_provide_webdriver_logged_in
            edit_issue_page = EditIssuePage(driver, issue_id).open()
            assertion("Edit issue page is opened", edit_issue_page.is_edit_issue_page(), is_(True),
                      "This is wrong page")
            validate_page_elements(edit_issue_page.page_elements)
            attach_screenshot_of_resized_page(driver, "Issue edit page is opened")
            if 'summary' in dict(fixture_update_issue_params).keys():
                edit_issue_page.edit_issue(summary = fixture_update_issue_params['summary'])
            if 'priority' in dict(fixture_update_issue_params).keys():
                edit_issue_page.edit_issue(priority = fixture_update_issue_params['priority'])
            if 'assignee' in dict(fixture_update_issue_params).keys():
                edit_issue_page.edit_issue(assign_to_me = True)
            logger.info("Step 'Updating the issue' finished")

        with allure.step("Validating updated issue"):
            logger.info("Step 'Validating updated issue' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response_get_issue = self.API_HTTP.get(
                    url_part = self.ROUTES['issues']['edit']['path'].format(id = issue_id))
            attach_json(dict(response_get_issue.headers), "Get issue Response headers")
            attach_json(list(response_get_issue.cookies.items()), "Get issue Response cookies")
            attach_json(json.loads(response_get_issue.content), "Get issue Response body")
            verify_http_status_code(response_get_issue.status_code, 200)
            if 'summary' in dict(fixture_update_issue_params).keys():
                assertion("Check that summary was updated",
                          json.loads(response_get_issue.content)['fields']['summary'],
                          is_(fixture_update_issue_params['summary']), "Summary wasn't updated")
            if 'priority' in dict(fixture_update_issue_params).keys():
                assertion("Check that priority was updated",
                          json.loads(response_get_issue.content)['fields']['priority']['name'],
                          is_(fixture_update_issue_params['priority']), "Priority wasn't updated")
            if 'assignee' in dict(fixture_update_issue_params).keys():
                assertion("Check that assignee was updated",
                          json.loads(response_get_issue.content)['fields']['assignee']['name'],
                          is_(fixture_update_issue_params['assignee']), "Priority wasn't updated")
            logger.info("Step 'Validating updated issue' finished")
        logger.info("Test 'Update UI issue' finished")
