#!/usr/bin/env python3
"""
Contains Elements for the Jira Dashboard page
"""
import logging

from selenium.webdriver.common.by import By

from project.tests.elements.base_element import Element

logger = logging.getLogger(__name__)


class DashboardPageElements(Element):

    def __init__(self, driver = None):
        super(DashboardPageElements, self).__init__(driver)
        self.driver = driver
        self.dashboard_caption_txt = Element(self.driver, (By.CLASS_NAME, "aui-page-header-main", "Dashboard caption"))
        self.dashboard_caption_menu_btn = \
            Element(self.driver, (By.CLASS_NAME, "aui-dropdown2-trigger-arrowless", "Dashboard management menu"))

        self.popup_close_btn = Element(self.driver, (By.CLASS_NAME, "icon-close", "Pop-up close button", 5))

        self.all_elements = [self.dashboard_caption_menu_btn, self.dashboard_caption_txt]
