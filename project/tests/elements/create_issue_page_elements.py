#!/usr/bin/env python3
"""
Contains Elements for the Jira Create Issue page
"""
import logging

from selenium.webdriver.common.by import By

from project.tests.elements.base_element import Element

logger = logging.getLogger(__name__)


class CreateIssuePageElements(Element):

    def __init__(self, driver = None):
        super(CreateIssuePageElements, self).__init__(driver)
        self.driver = driver
        self.form_caption_txt = Element(self.driver, (By.CSS_SELECTOR,
                                                      "#create-issue-dialog > .jira-dialog-heading > h2",
                                                      "Create Issue form caption"))
        self.project_name_input = Element(self.driver, (By.ID, "project-field", "Input field for project name"))
        self.issue_type_input = Element(self.driver, (By.ID, "issuetype-field", "Input field for issue type name"))
        self.summary_input = Element(self.driver, (By.ID, "summary", "Input field for issue summary"))
        self.priority_input = Element(self.driver, (By.ID, "priority-field", "Input field for issue priority"))
        self.assign_to_me_link = Element(self.driver,
                                         (By.ID, "assign-to-me-trigger", "Hyperlink to assign issue to me"))
        self.assignee_input = Element(self.driver, (By.ID, "assignee-field", "Input field for assignee name"))
        self.submit_btn = Element(self.driver, (By.ID, "create-issue-submit", "Button to submit the form"))

        self.error_txt = Element(self.driver, (By.CLASS_NAME, "error", "Error message text"))

        self.all_elements = [self.form_caption_txt, self.project_name_input, self.issue_type_input, self.summary_input,
                             self.priority_input, self.assign_to_me_link, self.assignee_input, self.submit_btn]
