#!/usr/bin/env python3
"""
Contains Elements for the Jira Issues page
"""
import logging

from selenium.webdriver.common.by import By

from project.tests.elements.base_element import Element

logger = logging.getLogger(__name__)


class IssuesPageElements(Element):

    def __init__(self, driver = None):
        super(IssuesPageElements, self).__init__(driver)
        self.driver = driver
        self.text_search_input = Element(self.driver, (By.ID, "searcher-query", "'Contains text' search field"))

        self.empty_results_txt = Element(self.driver, (By.CSS_SELECTOR, ".empty-results h3", "Empty results text"))
        self.found_issues_txt = Element(self.driver, (By.CSS_SELECTOR, ".issue-table-info-bar .results-count-total",
                                                      "Found issues counter"))
        self.first_found_issue = Element(
                self.driver, (By.XPATH, "//table[@id='issuetable']//tr[1]/td[@class='issuekey']/a[@class='issue-link']",
                              "1st issue in table with found issues"))

        self.all_elements = [self.text_search_input]
