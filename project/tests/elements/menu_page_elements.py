#!/usr/bin/env python3
"""
Contains Elements for different Jira menu pages
"""
import logging

from selenium.webdriver.common.by import By

from project.tests.elements.base_element import Element

logger = logging.getLogger(__name__)


class MenuPageElements(Element):

    def __init__(self, driver = None):
        super(MenuPageElements, self).__init__(driver)
        self.driver = driver
        self.issues_menu_collection = Element(self.driver, (By.ID, "find_link", "Issues menu collection"))
        self.create_issue_button = Element(self.driver, (By.ID, "create-menu", "Create issue button"))
        self.user_profile_menu_collection = Element(self.driver, (By.ID, "header-details-user-fullname",
                                                                  "User profile menu"))
        self.issue_created_popup_lnk = \
            Element(self.driver, (By.CLASS_NAME, "issue-created-key", "Issue created pop-up"))

        self.all_elements = [self.issues_menu_collection, self.create_issue_button, self.user_profile_menu_collection]


class IssuesMenuPageElements(MenuPageElements):

    def __init__(self, driver = None):
        super(IssuesMenuPageElements, self).__init__(driver)
        self.driver = driver
        self.reported_by_me_menu_item = Element(self.driver, (By.ID, "filter_lnk_reported_lnk",
                                                              "'Reported by me' menu item"))

        self.all_elements = [self.reported_by_me_menu_item]


class UserMenuPageElements(MenuPageElements):

    def __init__(self, driver = None):
        super(UserMenuPageElements, self).__init__(driver)
        self.driver = driver
        self.user_profile_menu_expanded_collection = Element(
                self.driver, (By.ID, "user-options-content", "User sub-menu collection"))
        self.logout_menu_item = Element(self.driver, (By.ID, "log_out", "'Log Out' menu item"))

        self.all_elements = [self.user_profile_menu_expanded_collection, self.logout_menu_item]
