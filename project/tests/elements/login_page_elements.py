#!/usr/bin/env python3
"""
Contains Elements for the Jira Login page
"""
import logging

from selenium.webdriver.common.by import By

from project.tests.elements.base_element import Element

logger = logging.getLogger(__name__)


class LoginPageElements(Element):

    def __init__(self, driver = None):
        super(LoginPageElements, self).__init__(driver)
        self.driver = driver
        self.username_fld = Element(self.driver, (By.ID, "login-form-username", "Username input field"))
        self.password_fld = Element(self.driver, (By.ID, "login-form-password", "Password input field"))
        self.login_btn = Element(self.driver, (By.ID, "login-form-submit", "Login button"))
        self.help_link = Element(self.driver, (By.ID, "login-form-cancel", "Can't access your account?"))
        self.error_message_txt = \
            Element(self.driver, (By.CLASS_NAME, "aui-message",
                                  "Sorry, your username and password are incorrect - please try again."))

        self.all_elements = [self.username_fld, self.password_fld, self.login_btn, self.help_link]
