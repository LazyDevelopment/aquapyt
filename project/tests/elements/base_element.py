#!/usr/bin/env python3
"""
Base element with common fields definitions
"""
import logging
import time

import selenium.webdriver.support.expected_conditions as ec
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium_utils import element

from project.utils.config_parser import read_ini_file

logger = logging.getLogger(__name__)


class Element(object):
    """
    Base element with common fields definitions
    """
    DEFAULT_TIMEOUT = int(read_ini_file('../tests_config.ini')['UI']['timeout'])


    def __get__(self, instance, owner):
        self._driver = instance.driver
        return self


    def __init__(self, driver, locator: tuple = None):
        """
        Creates new element with given locator

        :param driver: instance of WebDriver
        :param locator: tuple(selenium.By instance, value str for that By, Optional description str,
            Optional timeout int)
        """
        self._driver = driver
        self._locator = locator
        self._element = None
        self.raw = None
        self.loc_by = ""
        self.loc_value = ""
        self.loc_description = ""
        self.offset_pixels = 0
        self.timeout = self.DEFAULT_TIMEOUT
        self.locator(locator)


    def locator(self, locator: tuple):
        """
        Sets Selenium locator, description and timeout for current element from provided tuple.

        :param locator: Tuple with size of 2, 3, 4 variables, where (By locator strategy, value of the locator,
            optional description, optional timeout for waiting for this element)

        :return: element instance
        :raises ValueError: if tuple of wrong size or not tuple provided
        """
        if not self._locator == locator:
            self._locator = locator
        if not locator:
            logger.debug("Locator is None and will be set later")
        elif locator and len(locator) == 2:
            self.loc_by, self.loc_value = locator
        elif locator and len(locator) == 3:
            self.loc_by, self.loc_value, self.loc_description = locator
        elif locator and len(locator) == 4:
            self.loc_by, self.loc_value, self.loc_description, self.timeout = locator
        else:
            raise ValueError("Wrong size of provided tuple: 2, 3 or 4 items expected, but got {}"
                             .format(len(locator) or locator))
        return self


    def set_driver(self, dr):
        self._driver = dr
        return self


    def is_selected(self) -> bool:
        """
        Waits for element to be visible during its timeout or less, then returns True if it is selected or False if not

        :return: True if element is selected or False if not
        :raises TimeoutException: if element is not visible after timeout
        """
        self.wait_to_be_visible()
        return self._element.is_selected()


    def is_visible(self) -> bool:
        """
        Waits for element to be visible during its timeout or less, then returns True if it is displayed or False if not

        :return: True if element is displayed or False if not
        :raises TimeoutException: if element is not visible after timeout
        """
        try:
            self.wait_to_be_visible()
            return self._element.is_displayed()
        except TimeoutException:
            return False


    def is_value_in_attribute(self, attribute_name: str, attribute_value: str) -> bool:
        """
        Checks if the attribute value is present for given attribute

        :param attribute_name: attribute name e.g. "class"
        :param attribute_value: value in the class attribute that indicates the element is now active/opened
        :return: True if it is, False if not
        """
        return element.is_value_in_attr(self._element, attribute_name, attribute_value)


    def convert(self, web_element: WebElement, driver: WebDriver, locator: tuple = None):
        """
        Converts WebElement to Element

        :param web_element: WebElement to be converted
        :param driver: instance of WebDriver
        :param locator: Tuple with size of 2, 3, 4 variables, where (By locator strategy, value of the locator,
            optional description, optional timeout for waiting for this element)
        :return: instance of Element with converted WebElement inside
        """
        self._driver = driver
        self._element = web_element
        if locator:
            self.locator(locator)
        return self


    def format_locator(self, *args, **kwargs):
        """
        Applies Python string formatting feature to locators. As an example, you could define some locator string like
        "#{id}[{property}='{value}']" and then you could call this method to replace all placeholders to real values:
        format_locator(id = 'ID', property = 'property', value = 'value') and you'll receive Element with real locator
        inside

        :param args: placeholders' replacements
        :param kwargs: placeholders' replacements
        :return: Element with real locator
        """
        self.loc_value = self.loc_value.format(*args, **kwargs)
        return self


    @property
    def wd_wait(self) -> WebDriverWait:
        """
        :return: WebDriverWait instance for this Element
        """
        return WebDriverWait(self._driver, self.timeout)


    @property
    def action_chains(self) -> ActionChains:
        """
        :return: ActionChains instance for this Element
        """
        return ActionChains(self._driver)


    def find(self):
        """
        Waits for this Element to be visible on the page during its timeout or less, then finds it by Selenium

        :return: found Element
        :raises TimeoutException: if element is not visible after timeout
        """
        self.wait_to_be_visible()
        self.raw = element.get_when_visible(driver = self._driver, locator = (self.loc_by, self.loc_value),
                                            wait_seconds = self.timeout)
        self.convert(self.raw, self._driver)
        return self


    def find_deeper(self, locator):
        """
        Finds current element on the page, then finds other starting searching from current element

        :param locator: of the "deep" element
        :return: found Element
        """
        self.find()
        return Element.convert(self.raw.find_element(by = locator[0], value = locator[1]), locator, self._driver)


    def find_all(self):
        """
        Waits for at least 1 Element to be visible on the page during its timeout or less, then finds all elements
        for this locator by Selenium

        :return: Element with list of Elements in _element field
        :raises TimeoutException: if element is not visible after timeout
        """
        self.wait_to_be_visible()
        self.raw = element.get_when_all_visible(driver = self._driver, locator = (self.loc_by, self.loc_value),
                                                wait_seconds = self.timeout)
        self._element = [self.convert(elem, self._driver) for elem in self.raw]
        return self


    def extract(self):
        """
        :return: WebElement or list of WebElement items from current Element
        """
        return self._element


    def extract_raw(self):
        """
        :return: RAW data, got from Selenium; in most cases WebElement
        """
        return self.raw


    def click(self):
        """
        Waits for this Element to be clickable during its timeout or less, then tries for 10 times to click on it.
        Only 1 successful click will be performed.

        :return: None, as this is last action in chain
        :raises TimeoutException: if element is not clickable after timeout
        """
        from selenium.common.exceptions import ElementClickInterceptedException
        self.wait_to_be_clickable()
        for _ in range(10):
            try:
                element.click_on_staleable_element(driver = self._driver, el_locator = (self.loc_by, self.loc_value),
                                                   wait_seconds = self.timeout)
                break
            except ElementClickInterceptedException:
                time.sleep(0.5)


    def fill_with(self, text: str, keys: Keys = None):
        """
        Waits for this Element to be clickable during its timeout or less, then clears the input and sends keys of
        provided text into it. Or if Keys provided, clicks on element, selects all text inside by <Ctrl>+<A>
        combination, deletes it by <Del> key, sends keys of provided text into it and finally sends provided Keys there

        :param text: to be sent into element
        :param keys: keyboard keys to be sent into element
        :return: instance of this Element
        :raises TimeoutException: if element is not clickable after timeout
        """
        self.wait_to_be_clickable()
        if keys:
            from selenium.webdriver.common import action_chains
            element.click_on_staleable_element(driver = self._driver, el_locator = (self.loc_by, self.loc_value),
                                               wait_seconds = self.timeout)
            action_chains.ActionChains(self._driver).key_down(Keys.CONTROL).send_keys('a') \
                .key_up(Keys.CONTROL).perform()
            action_chains.ActionChains(self._driver).send_keys(Keys.DELETE).perform()
            self._element.send_keys(text)
            action_chains.ActionChains(self._driver).send_keys(keys).perform()
            time.sleep(2)
        else:
            self.clear()
            self._element.send_keys(text)
            time.sleep(1)
        return self


    def clear(self):
        """
        Waits for this Element to be visible during its timeout or less, then scrolls page to show it on screen and
        clears its content

        :return: instance of this Element
        :raises TimeoutException: if element is not visible after timeout
        """
        self.move_to()
        self._element.clear()
        return self


    def get_text(self):
        """
        Waits for this Element to be visible during its timeout or less, then scrolls page to show it on screen and
        retrieves text from it

        :return: retrieved text from this Element
        :raises TimeoutException: if element is not visible after timeout
        """
        self.move_to()
        return self._element.text


    def move_to(self):
        """
        Waits for this Element to be visible during its timeout or less, then scrolls page to show it on screen

        :return: instance of this Element
        :raises TimeoutException: if element is not visible after timeout
        """
        self.wait_to_be_visible()
        element.scroll_into_view(self._driver, self._element, self.offset_pixels)
        return self


    def wait_to_be_visible(self):
        """
        Sets Selenium implicit wait to Element's timeout, waits for "presence_of_element_located",
        "wait_until_stops_moving" and again for "presence_of_element_located" during its timeout or less

        :return: instance of this Element
        :raises TimeoutException: if element not satisfies expectations after timeout
        """
        self._driver.implicitly_wait(self.timeout)
        self._element = self.wd_wait.until(
                ec.presence_of_element_located((self.loc_by, self.loc_value)))
        element.wait_until_stops_moving(self._element, self.timeout)
        self._element = self.wd_wait.until(
                ec.presence_of_element_located((self.loc_by, self.loc_value)))
        return self


    def wait_to_be_clickable(self):
        """
        Waits for this Element to be visible during its timeout or less, then scrolls page to show it on screen,
        sets Selenium implicit wait to Element's timeout, waits for "element_to_be_clickable"

        :return: instance of this Element
        :raises TimeoutException: if element not satisfies expectations after timeout
        """
        self.move_to()
        self._driver.implicitly_wait(self.timeout)
        self._element = self.wd_wait.until(ec.element_to_be_clickable((self.loc_by, self.loc_value)))
        return self


    def wait_for_text(self, text: str):
        """
        Waits for this Element to be visible during its timeout or less, then scrolls page to show it on screen,
        sets Selenium implicit wait to Element's timeout, waits for "wait_for_element_text", with provided text

        :param text: to be found in Element
        :return: instance of this Element
        :raises TimeoutException: if element not satisfies expectations after timeout
        """
        self.move_to()
        self._driver.implicitly_wait(self.timeout)
        element.wait_for_element_text(self._driver, (self.loc_by, self.loc_value), text, self.timeout)
        return self


    def wait_to_disappear(self):
        """
        Sets Selenium implicit wait to Element's timeout, waits for "invisibility_of_element_located"

        :return: instance of this Element
        :raises TimeoutException: if element is visible after timeout
        """
        self._driver.implicitly_wait(self.timeout)
        self._element = self.wd_wait.until(
                ec.invisibility_of_element_located((self.loc_by, self.loc_value)))
        return self


    def hover(self):
        """
        Waits for this Element to be visible during its timeout or less, then scrolls page to show it on screen,
        hovers mouse pointer over this Element

        :return: instance of this Element
        :raises TimeoutException: if element is not visible after timeout
        """
        self.move_to()
        element.hover_over_element(self._driver, self._element)
        return self
