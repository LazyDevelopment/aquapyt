#!/usr/bin/env python3
"""
Jira Menu page representation
"""
import logging

from project.tests.elements.menu_page_elements import MenuPageElements, UserMenuPageElements, IssuesMenuPageElements
from project.tests.pages.base_page import BasePage

logger = logging.getLogger(__name__)


class MenuPage(BasePage):
    """
    PageObject class contains elements and methods for Jira top menu items
    """


    def __init__(self, driver):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(MenuPage, self).__init__(driver)
        self.elems = MenuPageElements(self._driver)
        self.url = ""


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def is_menu_present(self):
        """
        Checks whether Jira menu is present on currently opened page in browser

        :return: True if it is, False if not
        """
        res = []
        for elem in self.page_elements:
            res.append(1) if elem.is_visible() else res.append(0)
        return (0 not in res) and len(res) == 3


class UserMenuPage(MenuPage):
    """
    PageObject class contains elements and methods for Jira user menu items
    """


    def __init__(self, driver):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(UserMenuPage, self).__init__(driver)
        self.elems = UserMenuPageElements(self._driver)
        self.url = ""


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def is_menu_present(self):
        """
        Checks whether Jira menu is present on currently opened page in browser

        :return: True if it is, False if not
        """
        res = []
        for elem in self.page_elements:
            res.append(1) if elem.is_visible() else res.append(0)
        return (0 not in res) and len(res) == 2


    def is_user_logged_in(self):
        """
        Checks whether Jira session is for logged in user

        :return: True if it is, False if not
        """
        return self.elems.logout_menu_item.is_visible()


class IssuesMenuPage(MenuPage):
    """
    PageObject class contains elements and methods for Jira Issues menu items
    """


    def __init__(self, driver):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(IssuesMenuPage, self).__init__(driver)
        self.elems = IssuesMenuPageElements(self._driver)
        self.url = ""


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def is_menu_present(self):
        """
        Checks whether Jira menu is present on currently opened page in browser

        :return: True if it is, False if not
        """
        res = []
        for elem in self.page_elements:
            res.append(1) if elem.is_visible() else res.append(0)
        return (0 not in res) and len(res) == 1


    def go_to_reported_by_me_issues(self):
        """
        Opens Jira Issues page with "Reported by me" filter set

        :return: None
        """
        self.elems.reported_by_me_menu_item.click()
        self.wait_4_request("appswitcher", self.DEFAULT_TIMEOUT)
