#!/usr/bin/env python3
"""
Jira Edit Issue page representation
"""
import logging

from project.tests.elements.edit_issue_page_elements import EditIssuePageElements
from project.tests.pages.base_page import BasePage
from project.utils.attachments import attach_screenshot_of_resized_page

logger = logging.getLogger(__name__)


class EditIssuePage(BasePage):
    """
    PageObject class contains elements and methods for Jira edit issue page
    """


    def __init__(self, driver, issue_id: str = ''):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(EditIssuePage, self).__init__(driver)
        self.elems = EditIssuePageElements(self._driver)
        self.url = "{0}/{1}".format(self.BASE_URL, self.ROUTES['issues']['edit']['ui']['path'].format(id = issue_id))


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def open(self):
        """
        Opens the page and waits for specific request's response to be received

        :return: instance of this PageObject
        """
        self.open_page(self.url)
        self.wait_4_request("user/mention/search", self.DEFAULT_TIMEOUT)
        return self


    def is_edit_issue_page(self):
        """
        Checks whether currently opened page in browser is our page

        :return: True if it is, False if not
        """
        return "Edit Issue" == self.elems.form_caption_txt.get_text()


    def edit_issue(self, summary: str = None, issue_type: str = None, priority: str = None, assign_to_me: bool = False,
                   should_succeed: bool = True):
        """
        Edits the issue by changing values of provided fields to provided values

        :param summary: str issue summary
        :param issue_type: str type of issue in Jira
        :param priority: str priority of issue in Jira
        :param assign_to_me: bool True if issue should be assigned to current user, False if not
        :param should_succeed: bool True if it is expected that issue should be created, False if error is expected
        :return: None
        """
        from selenium.webdriver.common.keys import Keys
        if summary:
            self.elems.summary_input.fill_with(summary)
        if issue_type:
            self.elems.issue_type_input.click()
            self.elems.issue_type_input.fill_with(issue_type, Keys.ENTER)
        if priority:
            self.elems.priority_input.click()
            self.elems.priority_input.fill_with(priority, Keys.ENTER)
        if assign_to_me:
            self.elems.assign_to_me_link.click()
        attach_screenshot_of_resized_page(self._driver, "Jira issue with changed field")
        del self._driver.requests
        self.elems.submit_btn.click()
        if should_succeed:
            self.wait_4_request("/lastVisited", self.DEFAULT_TIMEOUT)
