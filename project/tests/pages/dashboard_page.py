#!/usr/bin/env python3
"""
Jira Dashboard page representation
"""
import logging

from project.tests.elements.dashboard_page_elements import DashboardPageElements
from project.tests.pages.base_page import BasePage

logger = logging.getLogger(__name__)


class DashboardPage(BasePage):
    """
    PageObject class contains elements and methods for Jira dashboard page
    """


    def __init__(self, driver):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(DashboardPage, self).__init__(driver)
        self.elems = DashboardPageElements(self._driver)
        self.url = "{0}/{1}".format(self.BASE_URL, self.ROUTES['dashboard']['path'])


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def open(self):
        """
        Opens the page and waits for specific request's response to be received

        :return: instance of this PageObject
        """
        self.open_page(self.url)
        self.wait_4_request("date.js", self.DEFAULT_TIMEOUT)
        return self


    def is_dashboard_page(self):
        """
        Checks whether currently opened page in browser is our page

        :return: True if it is, False if not
        """
        return "System Dashboard" == self.elems.dashboard_caption_txt.get_text()
