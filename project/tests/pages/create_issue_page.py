#!/usr/bin/env python3
"""
Jira Create Issue page representation
"""
import logging

from project.tests.elements.create_issue_page_elements import CreateIssuePageElements
from project.tests.pages.base_page import BasePage
from project.utils.attachments import attach_screenshot_by_content

logger = logging.getLogger(__name__)


class CreateIssuePage(BasePage):
    """
    PageObject class contains elements and methods for Jira create issue page
    """


    def __init__(self, driver):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(CreateIssuePage, self).__init__(driver)
        self.elems = CreateIssuePageElements(self._driver)
        self.url = ""


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def is_create_issue_page(self):
        """
        Checks whether currently opened page in browser is our page

        :return: True if it is, False if not
        """
        return "Create Issue" == self.elems.form_caption_txt.get_text()


    def create_issue(self, summary: str, issue_type: str = 'Bug', priority: str = None, assign_to_me: bool = False,
                     should_succeed: bool = True):
        """
        Creates new issue in Jira with provided params

        :param summary: str issue summary
        :param issue_type: str type of issue in Jira
        :param priority: str priority of issue in Jira
        :param assign_to_me: bool True if issue should be assigned to current user, False if not
        :param should_succeed: bool True if it is expected that issue should be created, False if error is expected
        :return: None
        """
        from selenium.webdriver.common.keys import Keys
        self.wait_4_request("userpreferences/create", self.DEFAULT_TIMEOUT)
        del self._driver.requests
        self.elems.summary_input.fill_with(summary)
        self.elems.issue_type_input.click()
        self.elems.issue_type_input.fill_with(issue_type, Keys.ENTER)
        self.wait_4_request("publish/bulk", self.DEFAULT_TIMEOUT)
        del self._driver.requests
        if priority:
            self.elems.priority_input.click()
            self.elems.priority_input.fill_with(priority, Keys.ENTER)
            self.wait_4_request("publish/bulk", self.DEFAULT_TIMEOUT)
            del self._driver.requests
        if assign_to_me:
            self.elems.assign_to_me_link.click()
        attach_screenshot_by_content(self._driver.get_screenshot_as_png(), "Creating Jira issue")
        self.elems.submit_btn.click()
        if should_succeed:
            self.wait_4_request("/secure/QuickCreateIssue.jspa", self.DEFAULT_TIMEOUT)
