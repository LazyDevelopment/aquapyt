#!/usr/bin/env python3
"""
Jira Issues page representation
"""
import logging

from project.tests.elements.issues_page_elements import IssuesPageElements
from project.tests.pages.base_page import BasePage

logger = logging.getLogger(__name__)


class IssuesPage(BasePage):
    """
    PageObject class contains elements and methods for Jira issues page
    """


    def __init__(self, driver):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(IssuesPage, self).__init__(driver)
        self.elems = IssuesPageElements(self._driver)
        self.url = "{0}/{1}".format(self.BASE_URL, self.ROUTES['issues']['ui']['path'])


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def open(self):
        """
        Opens the page and waits for specific request's response to be received

        :return: instance of this PageObject
        """
        self.open_page(self.url)
        self.wait_4_request("appswitcher", self.DEFAULT_TIMEOUT)
        return self


    def is_issues_page(self):
        """
        Checks whether currently opened page in browser is our page

        :return: True if it is, False if not
        """
        return self.elems.text_search_input.is_visible()


    def filter_issues(self, searching_text: str, results_expected: bool = True):
        """
        Filters list of issues by provided searching text

        :param searching_text: str text to be searched for
        :param results_expected: bool True if at least 1 issue should be found, False if 0 issues is expected
        :return: instance of this PageObject
        """
        from selenium.webdriver.common.keys import Keys
        del self._driver.requests
        self.elems.text_search_input.fill_with(searching_text, Keys.ENTER)
        if results_expected:
            self.wait_4_request("subtask", self.DEFAULT_TIMEOUT)
            self.elems.first_found_issue.wait_to_be_clickable()
        else:
            self.wait_4_request("issueTable", self.DEFAULT_TIMEOUT)
            self.elems.empty_results_txt.get_text()
        del self._driver.requests
        return self
