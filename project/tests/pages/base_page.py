#!/usr/bin/env python3
"""
Main class for all pages in project with common config
"""
import logging
from selenium.common.exceptions import TimeoutException, NoAlertPresentException, UnexpectedAlertPresentException

logger = logging.getLogger(__name__)


def handle_popup(drv) -> bool:
    """
    Waits for 2 sec for alert, then tries 5 times to accept alert and if it founds an unexpected alert on 5th run and
    subsequent it continues while there are alerts to be closed

    :param drv: instance of WebDriver
    :return: True if alert was found; False if not
    """
    from selenium_utils import alert
    import time

    try:
        alert.wait_until_alert_is_present(drv, 2)
        for i in range(5):
            try:
                while alert.handle_alert(drv, True):
                    logger.debug("Alert was handled")
            except NoAlertPresentException:
                time.sleep(1)
            except UnexpectedAlertPresentException:
                if i == 4:
                    i -= 1
        del drv.requests
        drv.switch_to_default_content()
        return True
    except TimeoutException:
        return False


class BasePage(object):
    """
    Main class for all pages in project with common config
    """
    from selenium_utils import navigate
    from project.utils.config_parser import read_ini_file, read_yaml_file

    BASE_URL = read_ini_file('../tests_config.ini')['Jira']['base_url']
    ROUTES = dict(read_yaml_file('tests/data/routes.yaml'))
    DEFAULT_TIMEOUT = int(read_ini_file('../tests_config.ini')['UI']['timeout'])


    def __init__(self, driver):
        """
        Inits the class by setting driver

        :param driver: instance of SeleniumWire WebDriver
        """
        self._driver = driver


    def scroll_to(self, txt: str):
        """
        Scrolls page to some text

        :param txt: Text to be shown
        :return: None
        """
        self.navigate.scroll_to(self._driver, txt)


    def scroll_to_bottom(self):
        """
        Scrolls page to bottom

        :return: None
        """
        self.navigate.scroll_to_page_bottom(self._driver)


    def wait_4_request(self, url_part: str, timeout: int):
        """
        Waits for request with provided part of URL for provided timeout, if not found doesn't raise exception

        :param url_part: str part of URL to filter request
        :param timeout: int the maximum time to wait in seconds
        :return: instance of the page
        """
        try:
            self._driver.wait_for_request(url_part, timeout)
        except TimeoutException:
            logger.error("Request with '{0}' part of URL wasn't found during {1} sec".format(url_part, timeout))


    def open_page(self, url: str):
        """
        Opens the page in browser using provided URL and with closing all alerts before that if any

        :param url: of the page to be opened
        :return: instance of the page
        """
        try:
            self._driver.get(url)
            if handle_popup(self._driver):
                del self._driver.requests
                self._driver.get(url)
        except UnexpectedAlertPresentException:
            if handle_popup(self._driver):
                del self._driver.requests
                self._driver.get(url)
        return self
