#!/usr/bin/env python3
"""
Jira Login page representation
"""
import logging

from project.tests.elements.login_page_elements import LoginPageElements
from project.tests.pages.base_page import BasePage

logger = logging.getLogger(__name__)


class LoginPage(BasePage):
    """
    PageObject class contains elements and methods for Jira login page
    """
    from project.utils.config_parser import read_ini_file

    DEFAULT_USER = read_ini_file('../tests_config.ini')['Jira']['login']
    DEFAULT_PASSWORD = read_ini_file('../tests_config.ini')['Jira']['password']


    def __init__(self, driver):
        """
        Inits class, page elements object, sets URL

        :param driver: instance of SeleniumWire WebDriver
        """
        super(LoginPage, self).__init__(driver)
        self.elems = LoginPageElements(self._driver)
        self.url = "{0}/{1}".format(self.BASE_URL, self.ROUTES['login']['path'])


    @property
    def page_elements(self) -> list:
        """
        List of page elements that always exist on the page

        :return: list of Element instances
        """
        return self.elems.all_elements


    def open(self):
        """
        Opens the page and waits for specific request's response to be received

        :return: instance of this PageObject
        """
        self.open_page(self.url)
        self.wait_4_request("/rest/menu/latest/appswitcher", self.DEFAULT_TIMEOUT)
        return self


    def login_to(self, user: str = DEFAULT_USER, passwd: str = DEFAULT_PASSWORD):
        """
        Performs login to Jira with provided credentials

        :param user: str login
        :param passwd: str password
        :return: instance of this PageObject
        """
        self.elems.password_fld.fill_with(passwd)
        self.elems.username_fld.fill_with(user)
        self.elems.login_btn.click()
        self.wait_4_request("analytics", self.DEFAULT_TIMEOUT)
        return self


    def is_login_error_shown(self):
        """
        Checks whether error message is shown on the page

        :return: True if error is there, False if not
        """
        return "Sorry, your username and password are incorrect - please try again." \
               in self.elems.error_message_txt.get_text()


    def is_login_page(self):
        """
        Checks whether currently opened page in browser is our page

        :return: True if it is, False if not
        """
        return "login" in self._driver.current_url
