#!/usr/bin/env python3
"""
Base test with configurations for Jira API tests execution
"""
import logging

import allure
import pytest

from project.tests.global_test import GlobalTest
from project.utils.config_parser import read_ini_file

logger = logging.getLogger(__name__)


@pytest.mark.api
@allure.feature('API tests')
class BaseApiTest(GlobalTest):
    """
    Main class for Jira API tests in project.
    """


    @pytest.fixture()
    def clear_session(self):
        """ Clear session headers and cookies """
        logger.debug("'clear_session' fixture called")
        self.API_HTTP.session.headers.clear()
        self.API_HTTP.session.cookies.clear()


    @pytest.fixture(scope = 'class', autouse = True)
    def fixture_setup(self):
        from project.utils.logger_config import setup_logging
        logger.debug("'fixture_setup' fixture called")
        setup_logging()


    @pytest.fixture(params = GlobalTest._update_issue_params(), ids = GlobalTest._update_issue_names)
    def fixture_update_issue_json(self, request):
        import json
        from pprint import pformat
        logger.debug("'fixture_update_issue_json' fixture called with parameter {}".format(request.param))
        with open('tests/data/update_issue_template.json', 'rt') as file:
            template = json.load(file)
        data_item = dict(template)
        if 'summary' in dict(request.param['case']).keys():
            data_item['fields']['summary'] = "APIu{}".format(request.param['case']['summary'])
            data_item['fields'].pop('priority')
            data_item['fields'].pop('assignee')
        if 'priority' in dict(request.param['case']).keys():
            data_item['fields']['priority']['name'] = request.param['case']['priority']
            data_item['fields'].pop('summary')
            data_item['fields'].pop('assignee')
        if 'assignee' in dict(request.param['case']).keys():
            data_item['fields']['assignee']['name'] = read_ini_file('../tests_config.ini')['Jira']['login']
            data_item['fields'].pop('summary')
            data_item['fields'].pop('priority')
        logger.debug('Next JSON was prepared by fixture and will be passed to test: \n{}'.format(pformat(data_item)))
        return json.dumps(data_item)


    @pytest.fixture(params = GlobalTest._create_issue_params())
    def fixture_create_issue_json_positive(self, request):
        def cleanup():
            logger.debug("Finalizer for 'fixture_create_issue_json_positive' fixture called")
            self.cleanup_issues("summary ~ 'APIs*'")
            logger.debug("Finalizer for 'fixture_create_issue_json_positive' fixture finished")


        request.addfinalizer(cleanup)
        import json
        from pprint import pformat
        logger.debug("'fixture_create_issue_json_positive' fixture called with parameter {}".format(request.param))
        with open('tests/data/create_issue_template.json', 'rt') as file:
            template = json.load(file)
        project_key = read_ini_file('../tests_config.ini')['Jira']['project_key']
        data_item = dict(template)
        data_item['fields']['project']['key'] = project_key
        data_item['fields']['summary'] = "APIs{}".format(request.param['case']['summary'])
        data_item['fields']['description'] = \
            request.param['case']['description'] if request.param['case']['description'] else ''
        data_item['fields']['issuetype']['name'] = request.param['case']['name']
        logger.debug('Next JSON was prepared by fixture and will be passed to test: \n{}'.format(pformat(data_item)))
        return json.dumps(data_item)


    @pytest.fixture(params = GlobalTest._create_issue_params(is_positive = False))
    def fixture_create_issue_json_negative(self, request):
        import json
        from pprint import pformat
        logger.debug("'fixture_create_issue_json_negative' fixture called with parameter {}".format(request.param))
        with open('tests/data/create_issue_template.json', 'rt') as file:
            template = json.load(file)
        project_key = read_ini_file('../tests_config.ini')['Jira']['project_key']
        data_item = dict(template)
        data_item['fields']['project']['key'] = project_key
        data_item['fields']['summary'] = \
            request.param['case']['summary'] if request.param['case']['summary'] else ''
        data_item['fields']['description'] = \
            request.param['case']['description'] if request.param['case']['description'] else ''
        data_item['fields']['issuetype']['name'] = \
            request.param['case']['name'] if request.param['case']['name'] else ''
        logger.debug('Next JSON was prepared by fixture and will be passed to test: \n{}'.format(pformat(data_item)))
        return json.dumps(data_item)


    @pytest.fixture()
    def fixture_create_issues(self, request):
        def cleanup():
            logger.debug("Finalizer for 'fixture_create_issues' fixture called")
            self.cleanup_issues("summary ~ 'APIb*'")
            logger.debug("Finalizer for 'fixture_create_issues' fixture finished")


        request.addfinalizer(cleanup)
        return self.create_issues("API", 'b')


    @pytest.fixture()
    def fixture_update_issues(self, request):
        def cleanup():
            logger.debug("Finalizer for 'fixture_create_issues' fixture called")
            self.cleanup_issues("summary ~ 'APIu*'")
            logger.debug("Finalizer for 'fixture_create_issues' fixture finished")


        request.addfinalizer(cleanup)
        return self.create_issues("API", 'u')
