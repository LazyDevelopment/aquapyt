#!/usr/bin/env python3
"""
Tests fibonacci_generator.py
"""
import pytest

from project.src import request_fibonacci_sequence
from project.tests.base_unit_test import BaseUnitTest
from project.utils.asserts import *

logger = logging.getLogger(__name__)


@allure.feature('Phase 1')
@allure.story('Fibonacci Generator Unit test')
@pytest.mark.fibonacci
class TestFibonacciGenerator(BaseUnitTest):
    """
    Tests fibonacci_generator.py
    """


    @allure.title("get_fibonacci_sequence(['asd']) should return TypeError exception")
    def test_wrong_type_of_param(self):
        """
        get_fibonacci_sequence(['asd']) should return TypeError exception
        """
        logger.info("Test 'get_fibonacci_sequence(['asd']) should return TypeError exception' started")
        with pytest.raises(TypeError) as e_info:
            assertion("Calling get_fibonacci_sequence(['asd'])",
                      request_fibonacci_sequence(['asd']),
                      raises(TypeError),
                      "TypeError exception wasn't raised for call with [] value instead of int", e_info)
        logger.info("Test 'get_fibonacci_sequence(['asd']) should return TypeError exception' finished")


    @allure.title("get_fibonacci_sequence(-3) should return ValueError('Incorrect length') exception")
    def test_wrong_value(self):
        """
        get_fibonacci_sequence(-3) should return ValueError('Incorrect length') exception
        """
        logger.info("Test 'get_fibonacci_sequence(-3) should return ValueError('Incorrect length') exception' started")
        with pytest.raises(ValueError) as e_info:
            assertion("Calling get_fibonacci_sequence(-3)",
                      request_fibonacci_sequence(-3),
                      raises(ValueError, 'Incorrect length'),
                      "ValueError exception wasn't raised or text is incorrect for call with negative value")
        logger.info("Test 'get_fibonacci_sequence(-3) should return ValueError('Incorrect length') exception' finished")


    @allure.title("get_fibonacci_sequence(24) should return [] with Fibonacci sequence with length = 24")
    def test_correct_value(self):
        """
        get_fibonacci_sequence(24) should return [] with Fibonacci sequence with length = 24
        """
        logger.info(
                "Test 'get_fibonacci_sequence(24) should return [] with Fibonacci sequence with length = 24' started")
        fibonacci = request_fibonacci_sequence(24)
        assertion("Type of result is list and has 24 items inside",
                  fibonacci,
                  all_of(instance_of(list), has_length(24)),
                  "Returned value has incorrect type")
        assertion("1st and 2nd items are int(1)",
                  fibonacci[0],
                  all_of(is_(fibonacci[1]), is_(1)),
                  "Wrong 1st and\or 2nd values of sequence")
        for i in range(2, 24):
            assertion("Current item #{1} = {0} is sum of previous 2 items in sequence".format(fibonacci[i], i),
                      fibonacci[i],
                      is_(fibonacci[i - 2] + fibonacci[i - 1]),
                      "Item #{0} is not sum of 2 previous items".format(i))
        logger.info(
                "Test 'get_fibonacci_sequence(24) should return [] with Fibonacci sequence with length = 24' finished")
