#!/usr/bin/env python3
"""
CRUD Jira issues tests
"""

from project.tests.base_api_test import BaseApiTest
from project.utils.asserts import *
from project.utils.attachments import *
from project.utils.config_parser import read_ini_file

logger = logging.getLogger(__name__)


@allure.story('Jira issues creation')
class TestIssuesCreate(BaseApiTest):
    """
    Tests for Jira issues API creation functionality
    """


    @allure.title("Create API issue")
    def test_create_issue(self, fixture_login, fixture_create_issue_json_positive):
        """
        Create issue
        """
        logger.info("Test 'Create API issue' started")
        expected_response_headers = ["X-AREQUESTID", "X-ASESSIONID", "X-XSS-Protection", "X-Content-Type-Options",
                                     "X-Frame-Options", "Content-Security-Policy", "X-ASEN", "X-Seraph-LoginReason",
                                     "X-AUSERNAME", "Cache-Control", "Content-Type", "Transfer-Encoding", "Date"]

        with allure.step("Creating an issue"):
            logger.info("Step 'Creating an issue' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response = self.API_HTTP.post(url_part = self.ROUTES['issues']['create']['path'],
                                          content = fixture_create_issue_json_positive)
            attach_json(dict(response.headers), "Response headers")
            attach_json(list(response.cookies.items()), "Response cookies")
            attach_json(json.loads(response.content), "Response body")
            logger.info("Step 'Creating an issue' finished")

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            verify_http_status_code(response.status_code, 201)
            verify_response_headers(expected_response_headers, response.headers)
            assertion("ID should be positive integer", int(json.loads(response.content)['id']),
                      greater_than(0),
                      "ID of created issue is not integer or < 0")
            assertion("Key should be combination of project code and some index", json.loads(response.content)['key'],
                      all_of(instance_of(str),
                             contains_string(
                                     json.loads(fixture_create_issue_json_positive)['fields']['project']['key'])),
                      "Key doesn't contain project code")
            assertion("Self link is pointing to created issue by ID", json.loads(response.content)['self'],
                      is_('{}/{}'.format(self.API_HTTP.base_url,
                                         self.ROUTES['issues']['edit']['path']
                                         .format(id = json.loads(response.content)['id']))),
                      "Self link is wrong")
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'Create API issue' finished")


    @allure.title("Create API issue negative test")
    def test_create_issue_negative(self, fixture_login, fixture_create_issue_json_negative):
        """
        Create API issue negative test
        """
        logger.info("Test 'Create API issue negative test' started")
        expected_response_headers = ["X-AREQUESTID", "X-ASESSIONID", "X-XSS-Protection", "X-Content-Type-Options",
                                     "X-Frame-Options", "Content-Security-Policy", "X-ASEN", "X-Seraph-LoginReason",
                                     "X-AUSERNAME", "Cache-Control", "Content-Type", "Transfer-Encoding", "Date",
                                     "Content-Encoding", "Vary", "Connection"]

        with allure.step("Creating an issue"):
            logger.info("Step 'Creating an issue' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response = self.API_HTTP.post(url_part = self.ROUTES['issues']['create']['path'],
                                          content = fixture_create_issue_json_negative)
            attach_json(dict(response.headers), "Response headers")
            attach_json(list(response.cookies.items()), "Response cookies")
            attach_json(json.loads(response.content), "Response body")
            logger.info("Step 'Creating an issue' finished")

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            verify_http_status_code(response.status_code, 400)
            verify_response_headers(expected_response_headers, response.headers)
            assertion("'errorMessages' is present in response", dict(json.loads(response.content)).keys(),
                      has_item('errorMessages'),
                      "'errorMessages' is absent in response")
            assertion("'errors' is present in response", dict(json.loads(response.content)).keys(),
                      has_item('errors'),
                      "'errors' is absent in response")
            assertion("Error description is present in response", dict(json.loads(response.content)['errors']).keys(),
                      has_length(1),
                      "Error description is absent in response or unexpected: {}"
                      .format(str(json.loads(response.content)['errors'])))
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'Create API issue negative test' finished")


@allure.story('Jira issues manipulation')
class TestIssuesManipulate(BaseApiTest):
    """
    Tests for Jira issues manipulation functionality (search, update)
    """


    @allure.title("Search API issues")
    def test_search_issue(self, fixture_login, fixture_create_issues, correct_creds):
        """
        Search API issues
        """
        logger.info("Test 'Search API issues' started")
        expected_response_headers = ["X-AREQUESTID", "X-ASESSIONID", "X-XSS-Protection", "X-Content-Type-Options",
                                     "X-Frame-Options", "Content-Security-Policy", "X-ASEN", "X-Seraph-LoginReason",
                                     "X-AUSERNAME", "Cache-Control", "Content-Type", "Transfer-Encoding", "Date"]

        with allure.step("Searching an issue"):
            logger.info("Step 'Searching an issue' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response_1_issue = self.API_HTTP.search(url_part = self.ROUTES['search']['path'],
                                                    jql = "summary ~ 'APIbkkb 0123456789'&maxResults=1")
            attach_json(dict(response_1_issue.headers), "Search 1 issue Response headers")
            attach_json(list(response_1_issue.cookies.items()), "Search 1 issue Response cookies")
            attach_json(json.loads(response_1_issue.content), "Search 1 issue Response body")
            logger.info("Step 'Searching an issue' finished")

        with allure.step("Searching for 5 issues"):
            logger.info("Step 'Searching for 5 issues' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response_5_issues = self.API_HTTP.search(url_part = self.ROUTES['search']['path'],
                                                     jql = "reporter={}&summary ~ 'APIb*'&maxResults=5"
                                                     .format(correct_creds['login']))
            attach_json(dict(response_5_issues.headers), "Search 5 issues Response headers")
            attach_json(list(response_5_issues.cookies.items()), "Search 5 issues Response cookies")
            attach_json(json.loads(response_5_issues.content), "Search 5 issues Response body")
            logger.info("Step 'Searching for 5 issues' finished")

        with allure.step("Searching for 0 issues"):
            logger.info("Step 'Searching for 0 issues' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response_0_issues = self.API_HTTP.search(url_part = self.ROUTES['search']['path'],
                                                     jql = "reporter=notexist")
            attach_json(dict(response_0_issues.headers), "Search 0 issues Response headers")
            attach_json(list(response_0_issues.cookies.items()), "Search 0 issues Response cookies")
            attach_json(json.loads(response_0_issues.content), "Search 0 issues Response body")
            logger.info("Step 'Searching for 0 issues' finished")

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            verify_http_status_code(response_1_issue.status_code, 200)
            verify_http_status_code(response_5_issues.status_code, 200)
            verify_http_status_code(response_0_issues.status_code, 200)
            verify_response_headers(expected_response_headers, response_1_issue.headers)
            verify_response_headers(expected_response_headers, response_5_issues.headers)
            verify_response_headers(expected_response_headers, response_0_issues.headers)
            verify_iterable_contains_exact_num_items(json.loads(response_1_issue.content)['issues'], 1,
                                                     "'Find 1 issue' case")
            verify_iterable_contains_exact_num_items(json.loads(response_5_issues.content)['issues'], 5,
                                                     "'Find 5 issues' case")
            verify_iterable_contains_exact_num_items(json.loads(response_0_issues.content)['issues'], 0,
                                                     "'Find no issues' case")
            for issue in json.loads(response_1_issue.content)['issues']:
                assertion("Reporter is {}".format(correct_creds['login']), issue['fields']['reporter']['name'],
                          is_(correct_creds['login']),
                          "Wrong issue found: reporter is incorrect")
            for issue in json.loads(response_5_issues.content)['issues']:
                assertion("Reporter is {}".format(correct_creds['login']), issue['fields']['reporter']['name'],
                          is_(correct_creds['login']),
                          "Wrong issue found: reporter is incorrect")
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'Search API issues' finished")


    @allure.title("Update API issue")
    def test_update_issue(self, fixture_login, fixture_update_issues, correct_creds, fixture_update_issue_json):
        """
        Update API issue
        """
        logger.info("Test 'Update API issue' started")
        expected_response_headers = ["X-AREQUESTID", "X-ASESSIONID", "X-XSS-Protection", "X-Content-Type-Options",
                                     "X-Frame-Options", "Content-Security-Policy", "X-ASEN", "X-Seraph-LoginReason",
                                     "X-AUSERNAME", "Cache-Control", "Content-Type", "Date"]

        with allure.step("Getting ID of the issue to be updated"):
            logger.info("Step 'Getting ID of the issue to be updated' started")
            issue_id = self.get_id_of_issue_to_update(correct_creds['login'], 'APIu*')
            logger.info("Step 'Getting ID of the issue to be updated' finished. ID is {}".format(issue_id))

        with allure.step("Updating the issue"):
            logger.info("Step 'Updating the issue' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response_update_issue = self.API_HTTP.put(
                    url_part = self.ROUTES['issues']['edit']['path'].format(id = issue_id),
                    content = fixture_update_issue_json)
            attach_json(dict(response_update_issue.headers), "Update Response headers")
            attach_json(list(response_update_issue.cookies.items()), "Update Response cookies")
            logger.info("Step 'Updating the issue' finished")

        with allure.step("Getting updated the issue"):
            logger.info("Step 'Getting updated the issue' started")
            self.API_HTTP.session.headers['Content-Type'] = 'application/json'
            response_get_issue = self.API_HTTP.get(
                    url_part = self.ROUTES['issues']['edit']['path'].format(id = issue_id))
            attach_json(dict(response_get_issue.headers), "Get issue Response headers")
            attach_json(list(response_get_issue.cookies.items()), "Get issue Response cookies")
            attach_json(json.loads(response_get_issue.content), "Get issue Response body")
            logger.info("Step 'Getting updated the issue' finished")

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            verify_http_status_code(response_update_issue.status_code, 204)
            verify_http_status_code(response_get_issue.status_code, 200)
            verify_response_headers(expected_response_headers, response_update_issue.headers)
            verify_response_headers(expected_response_headers, response_get_issue.headers)

            if 'summary' in json.loads(fixture_update_issue_json)['fields'].keys():
                assertion("Check that summary was updated",
                          json.loads(response_get_issue.content)['fields']['summary'],
                          is_(json.loads(fixture_update_issue_json)['fields']['summary']),
                          "Summary wasn't updated")
            if 'priority' in json.loads(fixture_update_issue_json)['fields'].keys():
                assertion("Check that priority was updated",
                          json.loads(response_get_issue.content)['fields']['priority']['name'],
                          is_(json.loads(fixture_update_issue_json)['fields']['priority']['name']),
                          "Priority wasn't updated")
            if 'assignee' in json.loads(fixture_update_issue_json)['fields'].keys():
                assertion("Check that assignee was updated",
                          json.loads(response_get_issue.content)['fields']['assignee']['name'],
                          is_(json.loads(fixture_update_issue_json)['fields']['assignee']['name']),
                          "Priority wasn't updated")
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'Update API issue' finished")
