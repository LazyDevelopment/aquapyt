#!/usr/bin/env python3
"""
Jira login tests
"""
import pytest

from project.tests.base_api_test import BaseApiTest
from project.utils.asserts import *
from project.utils.attachments import *

logger = logging.getLogger(__name__)


@allure.story('Jira login')
class TestLogin(BaseApiTest):
    """
    Jira login tests
    """
    from project.utils.config_parser import read_ini_file

    _wrong_creds_list = [
        {'login': 'sdfgkjhgbskvyr', 'password': 'sdfvhgskfjhasflaufcbkasjfhbach'},
        {'login': str(read_ini_file('../tests_config.ini')['Jira']['login']), 'password': 'sdfvhgskfjhsjfhbach'},
        {'login': 'alkjdgjsgkshfg', 'password': str(read_ini_file('../tests_config.ini')['Jira']['password'])}
    ]


    def _id(self, value):
        return value['login']


    @allure.title("API Login with wrong credentials")
    @pytest.mark.parametrize("creds", _wrong_creds_list, ids = _id)
    def test_login_wrong_creds(self, creds):
        """
        API Login with wrong credentials
        """
        logger.info("Test 'API Login with wrong credentials: {}' started".format(creds['login']))
        login_response_headers = ["Cache-Control", "Content-Security-Policy", "Content-Type", "Date", "Expires",
                                  "Pragma", "WWW-Authenticate", "Transfer-Encoding", "X-AREQUESTID", "X-ASEN",
                                  "X-Content-Type-Options", "X-Frame-Options", "X-Seraph-LoginReason",
                                  "X-XSS-Protection"]

        with allure.step("Logging in to Jira with wrong credentials: login = {0}, password = {1}"
                                 .format(creds['login'], creds['password'])):
            logger.info("Step 'Logging in to Jira with wrong credentials: login = {}' started".format(creds['login']))
            response = self.API_HTTP.basic_auth(url_part = self.ROUTES['login']['path'],
                                                login = creds['login'],
                                                password = creds['password'])
            attach_json(dict(response.headers), "Login Response headers")
            attach_json(list(response.cookies.items()), "Login Response cookies")
            logger.info("Step 'Logging in to Jira with wrong credentials: login = {}' finished".format(creds['login']))

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            verify_http_status_code(response.status_code, 401)
            verify_response_headers(login_response_headers, response.headers)
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'API Login with wrong credentials: {}' finished".format(creds['login']))


    @allure.title("API Login with correct credentials")
    def test_login_correct(self, correct_creds):
        """
        API Login with correct credentials
        """
        logger.info("Test 'API Login with correct credentials' started")
        login_response_headers = ["Cache-Control", "Content-Security-Policy", "Content-Type", "Date", "Expires",
                                  "Pragma", "Set-Cookie", "Transfer-Encoding", "X-AREQUESTID", "X-ASEN", "X-ASESSIONID",
                                  "X-AUSERNAME", "X-Content-Type-Options", "X-Frame-Options", "X-Seraph-LoginReason",
                                  "X-XSS-Protection"]

        with allure.step("Logging in to Jira with correct credentials"):
            logger.info("Step 'Logging in to Jira with correct credentials' started")
            response = self.API_HTTP.basic_auth(url_part = self.ROUTES['login']['path'],
                                                login = correct_creds['login'],
                                                password = correct_creds['password'])
            attach_json(dict(response.headers), "Login Response headers")
            attach_json(list(response.cookies.items()), "Login Response cookies")
            logger.info("Step 'Logging in to Jira with correct credentials' finished")

        with allure.step("Assertions"):
            logger.info("Step 'Assertions' started")
            verify_http_status_code(response.status_code, 200)
            verify_response_headers(login_response_headers, response.headers)
            logger.info("Step 'Assertions' finished")
        logger.info("Test 'API Login with correct credentials' finished")
