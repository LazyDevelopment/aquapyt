#!/usr/bin/env python3
"""
Base class for all UI tests in project
"""
import logging

import allure
import pytest

from project.tests.global_test import GlobalTest
from project.utils.config_parser import read_ini_file

logger = logging.getLogger(__name__)


@pytest.mark.ui
@pytest.mark.nondestructive
@allure.feature('UI tests')
class BaseUiTest(GlobalTest):
    """
    Main class for UI tests in project.
    """
    from project.utils.wdm_config import configure_wdm

    BROWSERS = configure_wdm()
    DEFAULT_TIMEOUT = int(read_ini_file('../tests_config.ini')['UI']['timeout'])
    RESOLUTION = (int(read_ini_file('../tests_config.ini')['UI']['resolution-width']),
                  int(read_ini_file('../tests_config.ini')['UI']['resolution-height']))


    @pytest.fixture(scope = 'class', params = BROWSERS, ids = BROWSERS)
    def fixture_provide_webdriver(self, request):
        def close():
            from project.tests.pages.base_page import handle_popup
            logger.debug("Finalizer for WebDriver instance is called")
            driver.get("http://example.com")
            try:
                handle_popup(driver)
                driver.close()
            except Exception as e:
                logger.error("Next exception was caught when closing browser:\n{}".format(e))
            driver.quit()


        request.addfinalizer(close)
        from webdriver_manager.firefox import GeckoDriverManager
        from webdriver_manager.chrome import ChromeDriverManager
        from seleniumwire import webdriver
        logger.debug("'fixture_provide_webdriver' fixture called with parameter {}".format(request.param))
        driver = None
        if request.param == 'firefox':
            from selenium.webdriver.firefox.options import DesiredCapabilities
            ff_caps = DesiredCapabilities.FIREFOX.copy()
            ff_caps["unexpectedAlertBehaviour"] = "accept"
            driver = webdriver.Firefox(executable_path = GeckoDriverManager().install(), desired_capabilities = ff_caps)
        elif request.param == 'chrome':
            from selenium.webdriver.chrome.options import Options
            chrome_opts = Options()
            chrome_opts.add_argument("disable-infobars")
            driver = webdriver.Chrome(executable_path = ChromeDriverManager().install(), chrome_options = chrome_opts)
        driver.set_window_position(1, 1)
        driver.set_window_size(self.RESOLUTION[0], self.RESOLUTION[1])
        driver.set_script_timeout(self.DEFAULT_TIMEOUT)
        driver.set_page_load_timeout(self.DEFAULT_TIMEOUT)
        driver.implicitly_wait(self.DEFAULT_TIMEOUT)
        del driver.requests
        logger.debug("'fixture_provide_webdriver' fixture is finished and next driver will be provided: '{}'"
                     .format(driver.name))
        return driver


    @pytest.fixture(scope = 'class')
    def fixture_provide_webdriver_logged_in(self, fixture_provide_webdriver):
        from project.tests.pages.login_page import LoginPage
        from project.tests.pages.dashboard_page import DashboardPage
        from project.tests.pages.menu_page import MenuPage, UserMenuPage
        from project.utils.asserts import assertion, is_
        logger.debug("'fixture_provide_webdriver_logged_in' fixture called.")
        driver = fixture_provide_webdriver
        login_page = LoginPage(driver).open()
        assertion("The opened page is Login page", login_page.is_login_page(), is_(True), "Wrong page is opened")
        login_page.login_to(user = str(read_ini_file('../tests_config.ini')['Jira']['login']),
                            passwd = str(read_ini_file('../tests_config.ini')['Jira']['password']))
        dashboard_page = DashboardPage(driver)
        top_menu_page = MenuPage(driver)
        assertion("The opened page is Dashboard page", dashboard_page.is_dashboard_page(), is_(True),
                  "Wrong page is opened")
        assertion("Top menu is present", top_menu_page.is_menu_present(), is_(True),
                  "There is no menu on this page")
        top_menu_page.elems.user_profile_menu_collection.click()
        user_menu_page = UserMenuPage(driver)
        user_menu_page.elems.logout_menu_item.wait_to_be_visible()
        assertion("User menu is present", user_menu_page.is_menu_present(), is_(True),
                  "There is no user menu on this page")
        assertion("User is logged in", user_menu_page.is_user_logged_in(), is_(True),
                  "There is no 'Logout' button, so it seems that user is not logged in")
        del driver.requests
        logger.debug("'fixture_provide_webdriver_logged_in' fixture finished "
                     "and driver will be provided with logged in session.")
        return driver


    @pytest.fixture(params = GlobalTest._create_issue_params(), ids = GlobalTest._create_issue_names)
    def fixture_create_issue_params_positive(self, request):
        def cleanup():
            logger.debug("Finalizer for 'fixture_create_issue_params_positive' fixture called")
            self.cleanup_issues("summary ~ 'UIs*'")
            logger.debug("Finalizer for 'fixture_create_issue_params_positive' fixture finished")


        request.addfinalizer(cleanup)
        logger.debug("'fixture_create_issue_params_positive' fixture called with parameter {}".format(request.param))
        return request.param


    @pytest.fixture()
    def fixture_create_issues_ui(self, request):
        def cleanup():
            logger.debug("Finalizer for 'fixture_create_issues' fixture called")
            self.cleanup_issues("summary ~ 'UIb*'")
            logger.debug("Finalizer for 'fixture_create_issues' fixture finished")


        request.addfinalizer(cleanup)
        logger.debug("'fixture_create_issues_ui' fixture called")
        return self.create_issues("UI", 'b')


    @pytest.fixture()
    def fixture_update_issues_ui(self, request):
        def cleanup():
            logger.debug("Finalizer for 'fixture_create_issues' fixture called")
            self.cleanup_issues("summary ~ 'UIu*'")
            logger.debug("Finalizer for 'fixture_create_issues' fixture finished")


        request.addfinalizer(cleanup)
        logger.debug("'fixture_update_issues_ui' fixture called")
        return self.create_issues("UI", 'u')


    @pytest.fixture(autouse = True)
    def fixture_take_screenshot_on_test_failure(self, request, fixture_provide_webdriver):
        def take_screenshot():
            logger.debug("Finalizer for 'fixture_take_screenshot_on_test_failure' called")
            if drv and request.node.report:
                from project.utils.attachments import attach_screenshot_by_content
                try:
                    attach_screenshot_by_content(drv.get_screenshot_as_png(),
                                                 attachment_name = request.function.__name__)
                except Exception as ex:
                    logger.error("Unable to get or attach screenshot with next exception caught:\n{}".format(ex))


        request.addfinalizer(take_screenshot)
        logger.debug("'fixture_take_screenshot_on_test_failure' called")
        drv = fixture_provide_webdriver


    @pytest.fixture(params = GlobalTest._update_issue_params(), ids = GlobalTest._update_issue_names)
    def fixture_update_issue_params(self, request):
        from pprint import pformat
        logger.debug("'fixture_update_issue_params' fixture called with parameter {}".format(request.param))
        data_item = {}
        if 'summary' in dict(request.param['case']).keys():
            data_item['summary'] = "UIu{}".format(request.param['case']['summary'])
        if 'priority' in dict(request.param['case']).keys():
            data_item['priority'] = request.param['case']['priority']
        if 'assignee' in dict(request.param['case']).keys():
            data_item['assignee'] = read_ini_file('../tests_config.ini')['Jira']['login']
        logger.debug('Next dict was prepared by fixture and will be passed to test: \n{}'.format(pformat(data_item)))
        return data_item
