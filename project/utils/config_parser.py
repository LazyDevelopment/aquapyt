#!/usr/bin/env python3
"""
Provides function to parse ini file and get a dict with its content

Usage:
python config_parser.py <path to file>
"""
import logging
import os
from configparser import ConfigParser

logger = logging.getLogger(__name__)
ini_files_parsed = {}
yaml_files_parsed = {}


def read_ini_file(str_file_path: str) -> dict:
    """
    Reads config file from given path and returns it as a dict. Also caches all parsed files during session and returns
    them from cache for subsequent calls

    :param str_file_path: File path to ini file as a string
    :return: dict with ini content inside
    """
    file_path = os.path.abspath(str(str_file_path))
    if file_path in ini_files_parsed.keys():
        logger.debug("'{}' already parsed, will be returned from cache".format(file_path))
        return ini_files_parsed[file_path]

    logger.debug('Going to parse {}'.format(file_path))
    ini_file = ConfigParser()
    logger.debug('Reading the {}'.format(file_path))
    with open(file_path, 'rt') as file:
        ini_file.read_file(file)
    logger.debug('Converting the ConfigParser object with our ini to dict')
    config_dict = {sect: dict(ini_file.items(sect)) for sect in ini_file.sections()}
    config_dict.pop('root', None)
    ini_files_parsed[file_path] = config_dict
    return config_dict


def read_yaml_file(str_file_path: str) -> dict:
    """
    Reads config file from given path and returns it as a dict. Also caches all parsed files during session and returns
    them from cache for subsequent calls

    :param str_file_path: File path to yaml file as a string
    :return: dict with yaml content inside
    """
    import yaml
    file_path = os.path.abspath(str(str_file_path))
    if file_path in yaml_files_parsed.keys():
        logger.debug("'{}' already parsed, will be returned from cache".format(file_path))
        return yaml_files_parsed[file_path]

    logger.debug('Going to parse {}'.format(file_path))
    with open(file_path, 'rt') as file:
        yaml_dict = yaml.load(file)
        yaml_files_parsed[file_path] = yaml_dict
        return yaml_dict


if __name__ == '__main__':
    import sys
    from pprint import pprint

    try:
        _, _, file_extension = str(sys.argv[1]).rpartition('.')
        if file_extension == 'ini':
            pprint(read_ini_file(sys.argv[1]))
        elif file_extension == 'yaml':
            pprint(read_yaml_file(sys.argv[1]))
        else:
            print('Parsing {} type of files is not implemented')
            exit(1)
    except FileNotFoundError:
        pprint("Incorrect file path: {}".format(sys.argv[1]))
        exit(1)
    except IndexError:
        pprint('You have to provide correct path to 1 ini file as an argument')
        exit(1)
