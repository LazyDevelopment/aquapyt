#!/usr/bin/env python3
"""
Provides functionality to attach some data to allure reports
"""
import json
import logging
from pprint import pformat

import allure

logger = logging.getLogger(__name__)


def attach_json(json_attachment, attachment_name: str = "JSON attachment"):
    """
    Attach JSON object to report

    :param json_attachment: list, dict or str with json
    :param attachment_name: str name for report
    """
    if isinstance(json_attachment, (list, dict)):
        attachment = json.dumps(json_attachment, indent = 4)
    elif isinstance(json_attachment, str):
        attachment = json.dumps(json.loads(json_attachment), indent = 4)
    else:
        raise ValueError(
                'Unexpected type of attachment: expected list, dict, str; received {}'.format(type(json_attachment)))
    allure.attach(attachment, name = str(attachment_name), attachment_type = allure.attachment_type.JSON)


def attach_plain_text(attachment: str, attachment_name: str = "TEXT attachment"):
    """
    Attach TEXT/PLAIN string to report

    :param attachment: str with content
    :param attachment_name: str name for report
    """
    allure.attach(str(attachment), name = str(attachment_name), attachment_type = allure.attachment_type.TEXT)


def attach_screenshot_by_path(path_to_file: str, attachment_name: str = "Screenshot attachment"):
    """
    Attach Screenshot PNG to report

    :param path_to_file: the file with screenshot
    :param attachment_name: str name for report
    """
    import os
    allure.attach.file(source = str(os.path.abspath(path_to_file)), name = str(attachment_name),
                       attachment_type = allure.attachment_type.PNG, extension = 'png')


def attach_screenshot_of_resized_page(driver, attach_name: str = "Screenshot attachment"):
    """
    Attach Screenshot PNG of all the page, resized to size of the content, to report. Resizes page to some smaller
    height than page content in Chrome only

    :param driver: instance of WebDriver to operate with page size
    :param attach_name: str name for report
    """
    import os
    from selenium_utils import screenshot
    current_page_size = driver.get_window_size()
    attach_path = os.path.abspath('../xunit-reports/{}'
                                  .format('{0}{1}{2}{3}'.format(attach_name.replace(' ', '-'), '-', str(driver.name),
                                                                '.png')))
    screenshot.get_screenshot(driver, attach_path)
    driver.set_window_size(current_page_size['width'], current_page_size['height'])
    allure.attach.file(source = str(attach_path), name = str(attach_name),
                       attachment_type = allure.attachment_type.PNG, extension = 'png')


def attach_screenshot_by_content(screenshot_content, attachment_name: str = "Screenshot attachment"):
    """
    Attach Screenshot PNG to report

    :param screenshot_content: the content of screenshot, encoded in base64, usually got from WebDriver instance
    :param attachment_name: str name for report
    """
    allure.attach(body = screenshot_content, name = str(attachment_name),
                  attachment_type = allure.attachment_type.PNG, extension = 'png')


def attach_selenium_request_details(requests: list, url_part: str, page_name: str = ''):
    """
    Attaches content of request from browser to server identified by specified part of URL and its response to report

    :param requests: List[Request] with requests from browser, caught by seleniumwire proxy
    :param url_part: str to filter all caught requests and find 1st matched
    :param page_name: str to include to attachments' names
    :return: None
    """
    our_request = None
    for request in requests:
        if url_part in request.path:
            our_request = request
            break
    if our_request:
        attach_plain_text("{0} '{1}'".format(our_request.method or '', our_request.path or ''),
                          "{} request method and URL".format(page_name))
        attach_json(dict(our_request.headers or {}), "{} request headers".format(page_name))
        attach_plain_text(pformat(str(our_request.body or ''), 4), "{} request body".format(page_name))
        attach_json(dict(our_request.response.headers or {}), "{} response headers".format(page_name))
        attach_plain_text(pformat(str(our_request.response.body or ''), 4), "{} response body".format(page_name))
