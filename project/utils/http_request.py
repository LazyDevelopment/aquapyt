#!/usr/bin/env python3
"""
Provides functionality to interact with WebServices using HTTP requests
"""
import json
import logging
from pprint import pformat

import requests
import urllib3
from requests.adapters import HTTPAdapter

from project.utils.config_parser import read_ini_file

logger = logging.getLogger(__name__)
# disable requests warnings
urllib3.disable_warnings()

DEFAULT_HEADER = {'Content-Type': 'application/json'}
DEFAULT_TIMEOUT = int(read_ini_file('../tests_config.ini')['HTTP']['timeout'])


class BaseHttp(object):
    """
    Main BaseHttp interactions module
    """


    def __init__(self, headers: dict = None, cookies: list = None):
        """
        :param headers: default headers
        :param cookies: default cookies
        """
        self.session = requests.session()
        self.session.verify = False
        self.session.headers.update(DEFAULT_HEADER)
        self.base_url = ""
        self.token_upd = True
        self.__mount()
        if headers:
            self.session.headers.update(headers)
        if cookies:
            self.session.cookies.update(cookies)


    def _common_code(self, response: requests.Response):
        self.update_token(response) if self.token_upd else None

        logger.info("-" * 35)
        logger.info("Response Status code: {status_code}".format(status_code = response.status_code))
        logger.debug("Response cookies: \n{r_cookies}".format(r_cookies = pformat(response.cookies.get_dict())))
        logger.debug("Response headers: \n{r_headers}".format(r_headers = pformat(dict(response.headers))))
        try:
            logger.debug("Response body: \n{response_body}\n".format(response_body = pformat(response.json())))
        except ValueError:
            logger.debug("Response body: \n{response_body}\n".format(response_body = response.content))


    def _common_preparation(self, method_name: str, url_part: str, headers: dict = None, cookies: list = None, ) -> str:
        url = self._make_url(str(url_part))
        logger.info("*" * 35)
        logger.info("Starting <{0}> request to {1}".format(method_name, url))
        logger.debug(
                "Request cookies: \n{cookies}".format(cookies = pformat(cookies or self.session.cookies.get_dict())))
        logger.debug(
                "Request headers: \n{headers}".format(headers = pformat(dict(headers or self.session.headers))))
        return url


    def basic_auth(self, url_part: str, login: str, password: str, timeout: int = DEFAULT_TIMEOUT) -> requests.Response:
        """
        :param login: string login
        :param password: string password
        :param url_part: request uri part. Not needed to put full url
        :param timeout: int timeout for request
        :return: response of the request
        :rtype: requests.Response
        """
        from requests.auth import HTTPBasicAuth
        url = self._make_url(str(url_part))

        logger.info("*" * 35)
        logger.info("Starting <GET> request for basic auth to {}".format(url))

        self.session.headers.clear()
        self.session.cookies.clear()
        response = self.session.get(url, auth = HTTPBasicAuth(str(login), str(password)), timeout = int(timeout))
        self._common_code(response)
        return response


    def get(self, url_part: str, headers: dict = None, cookies: list = None, timeout: int = DEFAULT_TIMEOUT,
            **kwargs) -> requests.Response:
        """
        :param timeout: int timeout for request
        :param url_part: request uri part. Not needed to put full url
        :param cookies: custom request cookies
        :param headers: custom request headers
        :param kwargs: additional optional params for get
        :return: response of the request
        :rtype: requests.Response
        """
        url = self._common_preparation('GET', url_part, headers, cookies)
        response = self.session.get(url, cookies = cookies, headers = headers, timeout = timeout, **kwargs)
        self._common_code(response)
        return response


    def search(self, url_part: str, jql: str, headers: dict = None, cookies: list = None,
               timeout: int = DEFAULT_TIMEOUT, **kwargs) -> requests.Response:
        """
        :param jql: JQL to search
        :param timeout: int timeout for request
        :param url_part: request uri part. Not needed to put full url
        :param cookies: custom request cookies
        :param headers: custom request headers
        :param kwargs: additional optional params for get
        :return: response of the request
        :rtype: requests.Response
        """
        url_part_jql = '{0}?jql={1}'.format(url_part, jql)
        url = self._common_preparation('GET', url_part_jql, headers, cookies)
        response = self.session.get(url, cookies = cookies, headers = headers, timeout = timeout, **kwargs)
        self._common_code(response)
        return response


    def post(self, url_part: str, content: dict = None, headers: dict = None, cookies: list = None,
             timeout: int = DEFAULT_TIMEOUT, **kwargs) -> requests.Response:
        """
        :param timeout: int timeout for request
        :param url_part: request uri part. Not needed to put full url
        :param content: data to send
        :param cookies: custom request cookies
        :param headers: custom request headers
        :param args: additional optional params for post
        :param kwargs: additional optional params for post
        :return: response of the request
        :rtype: requests.Response
        """

        url = self._common_preparation('POST', url_part, headers, cookies)
        try:
            logger.debug(
                    "Request content: \n{content}".format(content = pformat(dict(content or kwargs.get("json")))))
        except ValueError:
            logger.debug(
                    "Request content: \n{content}".format(content = pformat(str(content or kwargs.get("json")))))
        response = self.session.post(url, data = content, cookies = cookies, headers = headers, timeout = timeout,
                                     **kwargs)
        self._common_code(response)
        return response


    def post_file(self, url_part: str, files_to_send: dict = None) -> requests.Response:
        """
        :param url_part: request uri part. Not needed to put full url
        :param files_to_send: dict like {'form field name': open('file path', 'rb')}
        :return: response of the request
        :rtype: requests.Response
        """

        self.session.headers.clear()
        url = self._common_preparation('POST (file)', url_part)
        logger.debug(
                "Request content: \n{content}".format(content = pformat(dict(files_to_send))))
        response = self.session.post(url, files = files_to_send)
        self._common_code(response)
        return response


    def put(self, url_part: str, content: dict = None, headers: dict = None, cookies: list = None,
            timeout: int = DEFAULT_TIMEOUT, **kwargs) -> requests.Response:
        """
        :param timeout: int timeout for request
        :param url_part: request uri part. Not needed to put full url
        :param content: data to send
        :param cookies: custom request cookies
        :param headers: custom request headers
        :param args: additional optional params for put
        :param kwargs: additional optional params for put
        :return: response of the request
        :rtype: requests.Response
        """
        url = self._common_preparation('PUT', url_part, headers, cookies)
        logger.debug(
                "Request content: \n{content}".format(content = pformat(json.loads(content or kwargs.get("json")))))
        response = self.session.put(url, data = content, cookies = cookies, headers = headers, timeout = timeout,
                                    **kwargs)
        self._common_code(response)
        return response


    def delete(self, url_part: str, headers: dict = None, cookies: list = None, timeout: int = DEFAULT_TIMEOUT,
               **kwargs) -> requests.Response:
        """
        :param timeout: int timeout for request
        :param url_part: request uri part. Not needed to put full url
        :param cookies: custom request cookies
        :param headers: custom request headers
        :param kwargs: additional optional params for delete
        :return: response of the request
        :rtype: requests.Response
        """
        url = self._common_preparation('DELETE', url_part, headers, cookies)
        response = self.session.delete(url, cookies = cookies, headers = headers, timeout = timeout, **kwargs)
        self._common_code(response)
        return response


    def options(self, url_part: str, headers: dict = None, cookies: list = None, timeout: int = DEFAULT_TIMEOUT,
                **kwargs) -> requests.Response:
        """
        :param timeout: int timeout for request
        :param url_part: request uri part. Not needed to put full url
        :param cookies: custom request cookies
        :param headers: custom request headers
        :param kwargs: additional optional params for options
        :return: response of the request
        :rtype: requests.Response
        """
        url = self._common_preparation('OPTIONS', url_part, headers, cookies)
        response = self.session.options(url, cookies = cookies, headers = headers, timeout = timeout, **kwargs)
        self._common_code(response)
        return response


    def __mount(self):
        """Retry logic"""
        retries = urllib3.Retry()
        self.session.mount('http://', HTTPAdapter(max_retries = retries))
        self.session.mount('https://', HTTPAdapter(max_retries = retries))


    def update_token(self, response):
        """
        :param response: requests.Response or str
        """

        if response and isinstance(response, str):
            self.session.headers.update({"X-Auth-Token": response})
        elif response.headers.get("X-Fresh-Auth-Token"):
            self.session.headers.update({"X-Auth-Token": response.headers.get("X-Fresh-Auth-Token")})


    def _make_url(self, url_part: str) -> str:
        return '{}/{}'.format(self.base_url, url_part)


    def set_default_endpoint(self, endpoint: str):
        """ Set default environment endpoint
            :param endpoint: endpoint uri string
            :return: BaseHttp object
            :rtype: BaseHttp
        """
        self.base_url = endpoint
        return self


    def _turn_off_token_update(self):
        self.token_upd = False


class HttpBackend(BaseHttp):
    """
    This class should be used in API tests
    """


    def __init__(self):
        super(HttpBackend, self).__init__()
        self.set_default_endpoint(str(read_ini_file('../tests_config.ini')['Jira']['base_url']))
