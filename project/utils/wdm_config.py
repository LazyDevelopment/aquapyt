#!/usr/bin/env python3
"""
Contains function to configure WebDriver-Manager before execute UI tests
"""
import base64
import logging
import os

if not __name__ == '__main__':
    from project.utils.config_parser import read_ini_file

logger = logging.getLogger(__name__)


def configure_wdm(path_to_config: str = '../.wdm/config.ini') -> list:
    """
    Reads config properties from config file, sets WDM env. vars and returns list of expected browsers

    :param path_to_config: str path to WebDriverManager config file
    :return: list of browsers for test execution
    """
    wdm_conf = read_ini_file(path_to_config)
    browsers = [browser for browser in str(read_ini_file('../tests_config.ini')['UI']['browsers']).split(', ')]
    os.environ["DRIVER_PATH"] = os.path.abspath(wdm_conf['Global']['driver_path'])
    os.environ["GH_TOKEN"] = base64.b64decode(wdm_conf['Global']['gh_token_b64']).decode('ascii')
    os.environ["VERSION"] = wdm_conf['Global']['version']
    os.environ["OFFLINE"] = wdm_conf['Global']['offline']
    return browsers


def init_browsers(browsers: list):
    """
    Starts each browser from provided list to download and install the driver (tries to do that for 5 times)

    :param browsers: list of str browser names
    :return: None
    """

    err = False
    try:
        _ = os.environ['WDM_INIT']
    except KeyError:
        for browser in browsers:
            logger.debug("Init {} driver".format(browser))
            driver = _install_driver(browser)
            try:
                driver.quit()
            except AttributeError:
                logger.error("Driver wasn't installed properly and browser {} wasn't started".format(browser))
                err = True
        if not err:
            os.environ['WDM_INIT'] = 'True'


def _install_driver(browser: str):
    from webdriver_manager.firefox import GeckoDriverManager
    from webdriver_manager.chrome import ChromeDriverManager
    from seleniumwire import webdriver
    from tarfile import ReadError

    driver = None
    for _ in range(5):
        try:
            if browser == 'firefox':
                driver = webdriver.Firefox(executable_path = GeckoDriverManager().install())
            elif browser == 'chrome':
                driver = webdriver.Chrome(executable_path = ChromeDriverManager().install())
            break
        except (EOFError, ReadError):
            logger.error("Unable to install WebDriver for browser {}. Tried 5 times".format(browser))
    return driver


if __name__ == '__main__':
    from logger_config import setup_logging
    from config_parser import read_ini_file

    setup_logging(default_path = '../logging.yaml', default_level = logging.DEBUG)
    logger.info("Configuring WDM")
    init_browsers(configure_wdm())
