#!/usr/bin/env python3
"""
Configures logging for the project.
"""

import logging.config
import os

try:
    from project.utils.config_parser import read_yaml_file
except ModuleNotFoundError:
    from config_parser import read_yaml_file


def setup_logging(default_path: str = '../logging.yaml', default_level = logging.INFO, env_key: str = 'LOG_CFG'):
    """
    Setup logging configuration

    :param default_path: str path to YAML with logging config
    :param default_level: logging.<LEVEL> for case where no config is provided
    :param env_key: str name of environment variable with path to YAML with logging config
        (overrides 'default_path' if exists)
    :return: None
    """
    path = os.path.abspath(default_path)
    print("Logger's config: {}".format(path))
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        config = read_yaml_file(path)
        log_dir, _, _ = str(dict(dict(dict(config).get('handlers')).get('file_handler')).get('filename')) \
            .rpartition('/')
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        logging.config.dictConfig(config)
    else:
        print("Config file wasn't found, using default configuration")
        logging.basicConfig(level = default_level)
