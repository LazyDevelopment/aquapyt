#!/usr/bin/env python3
"""
Collection of asserts, used in tests
"""
import logging

import allure
from hamcrest import *

logger = logging.getLogger(__name__)


@allure.step("Validate Selenium responses")
def verify_selenium_responses(requests, except_of: list = tuple()):
    """
    Verifies all selenium requests with responses for having status code < 400 except of requests to given URL parts

    :param requests: iterable with requests to be verified
    :param except_of: list with str URL parts that should be skipped
    """
    for req in requests:
        if req.response:
            for exc in except_of:
                if exc not in req.path:
                    assert_that(req.response.status_code, less_than(400),
                                "Status code of next request is bad :\n"
                                "Method: {0}, URL: {1},\n"
                                "Request headers: {2},\n"
                                "Response code: {3},\n"
                                "Response headers: {4},\n"
                                "Response body: {5}".format(req.method, req.path, req.headers, req.response.status_code,
                                                            req.response.headers, req.response.body))


@allure.step("Validate page elements")
def validate_page_elements(page_elements: list, except_of: list = tuple()):
    for element in page_elements:
        if element not in except_of:
            assert_that(element.is_visible(), is_(True), "Element '{}' is not visible".format(element.loc_description))


@allure.step("Assert response headers")
def verify_response_headers(expected_headers, actual_headers):
    for header in expected_headers:
        with allure.step("Verify that header '{}' exists in response".format(header)):
            assert_that(header, is_in(actual_headers), "Given header is not present in the response!")


@allure.step("Assert HTTP status code is {1}")
def verify_http_status_code(actual, expected):
    pattern = is_in if type(expected) == list else is_
    assert_that(actual, pattern(expected), "Wrong status code!")


def verify_iterable_contains_exact_num_items(iterable, num_fields = int, name = ''):
    with allure.step("All fields are present in JSON object {}".format(name)):
        assert_that(len(iterable), is_(num_fields),
                    "# of fields is not correct in JSON object {}!".format(name))


def assertion(name: str, *args, **kwargs) -> None:
    """Asserts that actual value satisfies matcher. (Can also assert plain
    boolean condition.)

    :param name: The name of step in allure report
    :param args: (actual): The object to evaluate as the actual value.
                (matcher): The matcher to satisfy as the expected condition.
                (reason str): Optional explanation to include in failure description.

    ``assert_that`` passes the actual value to the matcher for evaluation. If
    the matcher is not satisfied, an exception is thrown describing the
    mismatch.

    ``assert_that`` is designed to integrate well with PyUnit and other unit
    testing frameworks. The exception raised for an unmet assertion is an
    :py:exc:`AssertionError`, which PyUnit reports as a test failure.

    With a different set of parameters, ``assert_that`` can also verify a
    boolean condition:

    .. function:: assert_that(assertion[, reason])

    param assertion:  Boolean condition to verify.
    param reason:  Optional explanation to include in failure description.

    This is equivalent to the :py:meth:`~unittest.TestCase.assertTrue` method
    of :py:class:`unittest.TestCase`, but offers greater flexibility in test
    writing by being a standalone function.

    """
    with allure.step(name):
        assert_that(*args, **kwargs)
