#!/usr/bin/env python3
"""
Demonstrates few cases of 'fibonacci_generator.py' usage

Usage:
`python3 main.py`
"""
import logging
from pprint import pformat

from fibonacci_generator import get_fibonacci_sequence
from utils import setup_logging

logger = logging.getLogger(__name__)


def main():
    setup_logging()

    logger.info('===Execution started===')
    logger.info(pformat("Calling 'get_fibonacci_sequence()'. Result is: {}".format(get_fibonacci_sequence())))
    logger.info(pformat("Calling 'get_fibonacci_sequence(0)'. Result is: {}".format(get_fibonacci_sequence(0))))
    logger.info(pformat("Calling 'get_fibonacci_sequence(1)'. Result is: {}".format(get_fibonacci_sequence(1))))
    logger.info(pformat("Calling 'get_fibonacci_sequence(3)'. Result is: {}".format(get_fibonacci_sequence(3))))
    logger.info(pformat("Calling 'get_fibonacci_sequence(12)'. Result is: {}".format(get_fibonacci_sequence(12))))
    try:
        get_fibonacci_sequence(['asd'])
    except (ValueError, TypeError):
        logger.info(pformat("Called 'get_fibonacci_sequence(['asd'])'. "
                            "Result is: ValueError or TypeError exception raised as expected"))
    try:
        get_fibonacci_sequence(None)
    except TypeError:
        logger.info(pformat("Called 'get_fibonacci_sequence(None)'. Result is: TypeError exception raised as expected"))
    try:
        get_fibonacci_sequence(-5)
    except ValueError:
        logger.info(pformat("Called 'get_fibonacci_sequence(-5)'. Result is: ValueError exception raised as expected"))
    logger.info('===Execution finished===')


if __name__ == '__main__':
    main()
