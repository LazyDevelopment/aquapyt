#!/usr/bin/env python3
"""
Provides Fibonacci generator implementation, which returns list of integers with given length
"""
from project.src.fibonacci_generator import get_fibonacci_sequence

__all__ = ['request_fibonacci_sequence']


def request_fibonacci_sequence(int_length: int) -> list:
    """
    Returns Fibonacci sequence of given length
    :param int_length: positive integer length of requested sequence
    :return: list of integers with given length
    """
    return get_fibonacci_sequence(int_length)
