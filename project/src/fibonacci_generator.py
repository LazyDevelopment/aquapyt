#!/usr/bin/env python3
"""
Generates Fibonacci sequence of given length

Usage:
`python3 fibonacci_generator.py <+int length>`
"""
import logging

logger = logging.getLogger(__name__)


def get_fibonacci_sequence(int_length: int = 0) -> list:
    """
    Returns Fibonacci sequence of given length
    :param int_length: positive integer length of requested sequence
    :return: list of integers with given length
    """
    logger.debug('Received int_length = {}'.format(int_length))
    length = int(int_length)
    logger.debug('Converted to int: {}'.format(length))
    if int_length is None \
            or length < 0:
        logger.error('Incorrect length')
        raise ValueError('Incorrect length')
    if length == 0:
        logger.debug('Returning []')
        return []
    logger.debug('Calling generator and returning results')
    return list(_generate_sequence(length))


def _generate_sequence(int_length: int = 0) -> iter:
    """
    Generates Fibonacci sequence of given length
    :param int_length: positive integer length of requested sequence
    :return: Generator of integers with given length
    """
    first, second = 1, 1
    for _ in range(int_length):
        yield first
        first, second = second, first + second


if __name__ == '__main__':
    import sys
    from pprint import pformat
    from project.utils.logger_config import setup_logging

    setup_logging()
    try:
        logger.info(pformat(get_fibonacci_sequence(sys.argv[1])))
    except ValueError:
        logger.error("Incorrect length: {}".format(sys.argv[1]))
        exit(1)
    except IndexError:
        logger.error('You have to pass one positive integer as an argument, like: `python3 fibonacci_generator.py 12`')
        exit(1)
