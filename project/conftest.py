#!/usr/bin/env python3
"""
py.test configuration file
"""
import logging
from datetime import datetime

import pytest
from py._xmlgen import html

from project.utils.wdm_config import configure_wdm, init_browsers

logger = logging.getLogger(__name__)
init_browsers(configure_wdm())


# Configure simple HTML report
@pytest.mark.optionalhook
def pytest_html_results_table_header(cells):
    """Add 'Description' and 'Time' columns to simple HTML report's header"""
    logger.debug("Add 'Description' and 'Time columns to simple HTML report's header'")
    cells.insert(2, html.th('Description'))
    cells.insert(4, html.th('Time', class_ = 'sortable time', col = 'time'))
    cells.pop()


@pytest.mark.optionalhook
def pytest_html_results_table_row(report, cells):
    """Add 'Description' and 'Time' columns to simple HTML report's rows"""
    logger.debug("Add 'Description' and 'Time' columns to simple HTML report's rows")
    try:
        cells.insert(2, html.td(report.description))
    except AttributeError:
        cells.insert(2, html.td('There is no description'))
    cells.insert(4, html.td(datetime.utcnow(), class_ = 'col-time'))
    cells.pop()


@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    """Add doc string from tests to simple HTML report"""

    logger.debug("Add doc string from tests to simple HTML report")
    outcome = yield
    report = outcome.get_result()
    try:
        report.description = item.function.__doc__
    except AttributeError:
        report.description = 'There is no description'
    if report.failed and report.when == 'call':
        exc = call.excinfo.type
        if not (exc == AssertionError):  # , JsonValidationError - for future
            report.when = 'setup'  # dirty hack to tell allure that test failed on setup
            # to mark it as broken on non Assertion exceptions
        setattr(item, "report", report.when)
    else:
        setattr(item, "report", None)
    return report
